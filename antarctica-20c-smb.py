#!/usr/bin/env python
# encoding: utf-8
'''
antarctica-20c-smb.py
Created by Cécile Agosta on 30-06-2017
Last modified: 12-03-2018
2017 __ULiege__
'''
# = IMPORT MODULES ========================================================== #
# ipython -pylab for interactive plot
from __future__ import division # true division
from netCDF4 import Dataset
import datetime
import matplotlib.dates as mdates
import numpy as np # array module
import numpy.ma as ma # masked array
from StringIO import StringIO
from pylab import *
from time import * # module to measure time
import csv
from os.path import exists
from mpl_toolkits.axes_grid1 import host_subplot
from matplotlib.transforms import Bbox
from matplotlib.path import Path
from matplotlib.patches import Rectangle
from matplotlib.font_manager import FontProperties
from matplotlib import gridspec
import matplotlib.pyplot as plt
import operator
import array
import pandas as pd # Python Data Analysis Library
import scipy
# My modules
import shade
import curvature
import read_model
import read_obs
import extract_data
import sectors
import readwrite
# ========================================================================== #

# fixed variables #
# =============== #
MAR_rean_list = ['ERA-Interim','MERRA2','JRA-55'] # 'NCEP1','NCEP2','20CRv2
mod_dir = './data/model/'
obs_dir = './data/observation/'
# time axis
version = 'v3.6.41'
year_range = range(1979,2015+1)
dates_index = np.array([pd.datetime(year,month,15) for year in year_range for month in np.array(range(12))+1])
years_mar = np.array([di.year for di in dates_index])
obs_list = ('wang',)  # 'samba', 'wang' (2016 update of samba), 'sumup'
verbose = True
rean_list = MAR_rean_list + ['RACMO2']

# read models #
# =========== #
# read MAR #
# -------- #
# grid
grid2D = read_model.read_mar_grid()
x1D, y1D = grid2D['x'], grid2D['y']
x2D, y2D = grid2D['x2D'], grid2D['y2D']
# smb monthly
smb2D,mdtime={},{}
for rean in MAR_rean_list:
    smb2D[rean], mdtime[rean] = read_model.read_mar_monthly(rean)
# read RACMO2 #
# ----------- #
x2D_rac0, y2D_rac0, lsm2D_rac0, sh2D_rac0 = read_model.read_rac_grid()
# new lsm for both models
ice2D_rac = np.nan_to_num(read_model.regrid_rac_to_mar(lsm2D_rac0,x2D_rac0,y2D_rac0,x2D,y2D,None,None))
lsm2D_rac = np.nan_to_num(read_model.regrid_rac_to_mar(lsm2D_rac0,x2D_rac0,y2D_rac0,x2D,y2D,lsm2D_rac0,None))
lsm2D = (grid2D['ice']<0.5)|(ice2D_rac<0.5)|(lsm2D_rac==0)
# regrid racmo2 on mar grid (linear barycentric interpolation)
def regrid(var2D,mask_in=lsm2D_rac0,mask_out=lsm2D):
    return read_model.regrid_rac_to_mar(var2D,x2D_rac0,y2D_rac0,x2D,y2D,mask_in,mask_out)
# smb
smb2D['RACMO2'], mdtime['RACMO2'] = read_model.read_rac_monthly('smb',dates_index,lsm2D,regrid)
# SMB components #
# -------------- #
mean2D, std2D, mean3D = {}, {}, {}
# mar
for rean in MAR_rean_list:
    mean2D['mar_'+rean], std2D['mar_'+rean], mean3D['mar_'+rean] = {}, {}, {}
    for var_name in 'smb','snf','rnf','sbl','mlt','rof':
        mean2D['mar_'+rean][var_name], std2D['mar_'+rean][var_name], mean3D['mar_'+rean][var_name] = \
                read_model.read_mar_annual(var_name,lsm2D,rean)
# racmo2
mean2D['rac'], std2D['rac'], mean3D['rac'] = {}, {}, {}
for var_name in 'sbl','suds','susn','erds','rof','mlt','smb','snf','rnf':
    mean2D['rac'][var_name], std2D['rac'][var_name], mean3D['rac'][var_name] = read_model.read_rac_annual(var_name,lsm2D,regrid)
# add new variables #
# ----------------- #
def time_average(var3D,mask=lsm2D):
    return ma.array(var3D.mean(axis=0),mask=mask)
# low-level sublimation, Grazioli 2017 PNAS (estimated for 2015)
filename = obs_dir+'grazioli_pnas_MARgrid.nc'
prec_max, prec_ground = readwrite.read_nc(filename,['PREC_MAX','PREC_GROUND_CUMUL'])
Gsbl2D = ma.array(prec_max.sum(axis=0)-prec_ground,mask=lsm2D)
Gsnf2D = ma.array(prec_ground,mask=lsm2D)
Gratio2D = Gsbl2D / Gsnf2D
# wind speed
filename = mod_dir+'MAR-ERA-Interim_UV10m_20c_monthly.nc4'
ws2D = readwrite.read_nc(filename,['UV10m'])[0]
ws2D = ma.array(time_average(ws2D),mask=lsm2D)
# wind divergence
filename = mod_dir+'MAR-ERA-Interim_u10m_1979-2015_ave.nc4'
uu2D = readwrite.read_nc(filename,['u10m'])[0]
filename = mod_dir+'MAR-ERA-Interim_v10m_1979-2015_ave.nc4'
vv2D = readwrite.read_nc(filename,['v10m'])[0]
dudx, dvdy = np.gradient(uu2D)[1], np.gradient(vv2D)[0]
div2D = ma.array(dudx+dvdy,mask=lsm2D)
# define variables for easier use #
# ------------------------------- #
dh = (x1D[1]-x1D[0])/2.
ground2D = grid2D['ground']
sh2D = ma.array(grid2D['sh'],mask=lsm2D)
curv2D = ma.array(curvature.curvature(x1D,y1D,sh2D),mask=lsm2D)
snf2D_mar = mean2D['mar_ERA-Interim']['snf']
snf2D_rac = mean2D['rac']['snf']
dsnf2D = snf2D_mar - snf2D_rac
dsnf2D_rel = dsnf2D / snf2D_mar
smb2D_mar = mean2D['mar_ERA-Interim']['smb']
smb2D_rac = mean2D['rac']['smb']
dsmb2D =  smb2D_mar - smb2D_rac
dsmb2D_rel = dsmb2D / abs(smb2D_mar)
# update grid2D #
# ------------- #
grid2D['lsm'] = invert(lsm2D)
grid2D['rac_sh'] = regrid(sh2D_rac0,mask_in=None,mask_out=lsm2D)
grid2D['rac_curv'] = ma.array(curvature.curvature(x1D,y1D,grid2D['rac_sh']),mask=lsm2D)
grid2D['sh'] = sh2D
grid2D['subl'] = Gratio2D
grid2D['curv'] = curv2D
grid2D['ws'] = ws2D
grid2D['div'] = div2D

# read obs #
# ======== #
obs_data={}
for obs_name in obs_list:
    obs_data[obs_name] = read_obs.read_obs(obs_name)
obs_tmp = pd.concat([obs_data[obs_name] for obs_name in obs_list])
# concatenate observations on same locations
obs,ids=read_obs.one_obs_by_location(obs_tmp,yearlim=1950)

# extract model data on obs (XY/XYT) #
# ================================== #
# gridXY -> bilinear interpolation of model fields (2D) to the obs location (XY)
gridXY, ijw_dict, mask_dict = extract_data.grid_2DtoXY(grid2D,obs)
# add information to obs
obs['mar_i'], obs['mar_j'] = gridXY['mar_i'], gridXY['mar_j']
obs['mar_sh'] = gridXY['sh']
obs['obs_sh'] = obs['sh']
# smbXY -> bilinear interpolation + average over the obs time span
smbXY={}
for rean in rean_list:
    smbXY[rean] = extract_data.model_2DtoXY(rean,ijw_dict,mask_dict,smb2D,mdtime,obs)
# check variability
def check_annual_variability():
    figure()
    scatter(mean2D['mar_ERA-Interim']['smb'],std2D['mar_ERA-Interim']['smb'],s=4,c='b')
    scatter(smbXY[rean]['mean'],smbXY[rean]['std'],s=4,c='r')

# average on MAR grid (IJ) #
# ======================== #
# IJ -> average obs and model-on-obs (XYT) on mar grid cells (IJ)
# filter
dsh=abs(gridXY['sh']-obs['sh'])
startbefore79andnblt8=(obs['yearstart']<1979)&(obs['yearnb']<8)
marlsm=gridXY['lsm']
# suspicious obs
suspicious=(obs['mar_j']==74)&(obs['mar_i']==84)
selected=(obs['smb']!=0.)&(dsh<=200.)&(marlsm==1)&logical_not(startbefore79andnblt8)&logical_not(suspicious)
extract_data.check_select(obs,dsh,startbefore79andnblt8,marlsm,suspicious)
# define regions
basin_name, location_name, region_name = extract_data.define_regions()
# gridIJ
sobs = obs[selected]
sgridXY = gridXY[selected]
smbXY_select = {rean:smbXY[rean][selected] for rean in rean_list}
gridIJ = extract_data.grid_XYtoIJ(sobs,sgridXY,smbXY_select,grid2D)

###########
## Plots ##
###########

# mass balance by basin #
# --------------------- #
basin2D = ma.array(grid2D['basin'],mask=grid2D['basin']<0)
mask_tis = lsm2D
mask_gis = invert((grid2D['basin']>=0)&(grid2D['lsm']==1))
mask_pis = invert(((grid2D['basin']==15)|(grid2D['basin']==16)|(grid2D['basin']==17))&(grid2D['lsm']==1))
mask_islands = invert((grid2D['basin']==0)&(grid2D['lsm']==1))
mask_gis_nopis = mask_gis|invert(mask_pis)
mask_wais = invert((grid2D['basin']==1)|(grid2D['basin']==2)|\
        (grid2D['basin']==9)|(grid2D['basin']==14)|\
        (grid2D['basin']==18)&(grid2D['lsm']==1))
mask_eais = mask_gis|invert(mask_wais)|invert(mask_pis)|invert(mask_islands)
mask_tis_nopis = lsm2D|invert(mask_pis)|((grid2D['lat']>-74)&(grid2D['lon']>-80)&(grid2D['lon']<-50))

def total_mass_balance(smb_var,mask):
    ice05 = (ma.array(smb_var,mask=(mask|lsm2D))*grid2D['area']).sum()*10**(-6)
    return ice05

def annual_mass_balance(smb3D,mask):
    if smb3D.ndim!=3:
        print "Error in annual_mass_balance, variable must have 3 dimensions (t,y,x)"
        return None
    tmb = []
    for tt in range(smb3D.shape[0]):
        tmb.append(total_mass_balance(smb3D[tt,:,:],mask))
    return np.array(tmb)

def total_area(mask):
    ice05 = (ma.array(grid2D['area'],mask=(mask|lsm2D))).sum()*10**(-6)
    return round(ice05,2)

def compute_theoritical_subl():
    ground = total_mass_balance(snf2D_mar,mask_tis_nopis)
    # subl = 0.17 * max
    # subl = max - ground
    # => subl = (1/0.17) subl - ground
    # => subl = ground / (1/0.17 - 1)
    subl = ground / (1/0.17 - 1)

def check_racmo_numbers():
    print 'wais SMB: ',round(total_mass_balance(smb2D_rac,mask_wais)-644)
    print 'eais SMB: ',round(total_mass_balance(smb2D_rac,mask_eais)-1130)
    print 'totis SMB: ',round(total_mass_balance(smb2D_rac,mask_tis_nopis)-2229)
    print 'totis snf: ',round(total_mass_balance(snf2D_rac,mask_tis_nopis)-2394)

def compute_total_sbl(snf2D,sbl2D,sh,ratio):
    sbl2D_rel = sbl2D / snf2D
    sbl = total_mass_balance(sbl2D,mask_tis_nopis|(sbl2D_rel<ratio)|(sh2D>sh))
    tot = total_mass_balance(snf2D,mask_tis_nopis|(sh2D>sh))
    return int(round(sbl)),int(round(sbl/tot*100.))

def show_sbl(sh=5000,ratio=0.15,show=True):
    # dsnf
    dsnf, dsnf_rel = compute_total_sbl(snf2D_mar,maximum(-dsnf2D,0),sh=sh,ratio=ratio)
    # dsnf 2015
    dsnf_annual, dsnf_annual_rel = [], []
    for i,year in enumerate(year_range):
        snf2D_mar_year = mean3D['mar_ERA-Interim']['smb'][i,:,:]
        snf2D_rac_year = mean3D['rac']['smb'][i,:,:]
        dsnf2D_year = snf2D_rac_year - snf2D_mar_year
        dsnf_year, dsnf_year_rel = compute_total_sbl(snf2D_mar_year,maximum(dsnf2D_year,0),sh=sh,ratio=ratio)
        dsnf_annual.append(dsnf_year)
        dsnf_annual_rel.append(dsnf_year_rel)
    dsnf_annual = np.array(dsnf_annual)
    dsnf_annual_rel = np.array(dsnf_annual_rel)
    dsnf_2015, dsnf_2015_rel = dsnf_annual[-1], dsnf_annual_rel[-1]
    #Grazioli
    Gsbl_2015, Gsbl_2015_rel = compute_total_sbl(Gsnf2D,Gsbl2D,sh=sh,ratio=ratio)
    # print
    if show:
        def print_sbl(subl,subl_rel,label):
            print label+':',int(round(subl)),'Gt/year ('+str(int(round(subl_rel))),'%)'
        print
        print 'sh<'+str(int(round(sh)))+', ratio='+str(int(ratio*100))+'%:'
        print_sbl(dsnf,dsnf_rel,'1979-2015')
        print_sbl(dsnf_2015,dsnf_2015_rel,'2015')
        print_sbl(Gsbl_2015,Gsbl_2015_rel,'Grazioli 2015')
    return dsnf, int(round(dsnf_annual.std())), dsnf_2015, Gsbl_2015

def compute_sbl_tab():
    show_sbl(sh=5000,ratio=0.15)
    show_sbl(sh=2000,ratio=0.15)
    show_sbl(sh=1000,ratio=0.15)
    # uncertainties on ratio
    def sbl_uncertainty(sh,ratio):
        dsnf_m, std_m, dsnf_2015_m, Gsbl_2015_m = show_sbl(sh=sh,ratio=ratio+0.05,show=False)
        dsnf_p, std_p, dsnf_2015_p, Gsbl_2015_p = show_sbl(sh=sh,ratio=ratio-0.05,show=False)
        dsnf_pm, dsnf_2015_pm, Gsbl_2015_pm = dsnf_p-dsnf_m, dsnf_2015_p-dsnf_2015_m, Gsbl_2015_p-Gsbl_2015_m
        return dsnf_pm/2., dsnf_2015_pm/2., Gsbl_2015_pm/2.
    sbl_uncertainty(5000,0.15)
    # sbl = f(sh)
    sh_list = linspace(100,4000,40)
    dsnf_sh = np.array([show_sbl(sh,ratio=0.2,show=False)[0] for sh in sh_list])
    Gsbl_sh = np.array([show_sbl(sh,ratio=0.2,show=False)[2] for sh in sh_list])
    ratio_list = linspace(0,1.,40)
    dsnf_ratio = np.array([show_sbl(5000,ratio=ratio,show=False)[0] for ratio in ratio_list])
    Gsbl_ratio = np.array([show_sbl(5000,ratio=ratio,show=False)[2] for ratio in ratio_list])
    figure()
    clf()
    plot(sh_list,dsnf_sh)
    plot(sh_list,Gsbl_sh)
    figure()
    clf()
    plot(ratio_list,dsnf_ratio,label='dsnf')
    plot(ratio_list,Gsbl_ratio,label='Grazioli')
    legend()

# shade results #
# ------------- #
'''
define the shade frame for squared Antarctica
'''
def layout(scale=True,axis_minmax=True):
    return shade.layout(x2D,y2D,grid2D['lon'],grid2D['lat'],grid2D['sh'],lsm2D,grid2D['ground'],scale=scale,axis_minmax=axis_minmax)
def scontour(var2D,levels,lw=1.,zorder=10,lc='k'):
    return shade.scontour(x2D,y2D,var2D,levels,lw=lw,zorder=zorder,lc=lc)
def spcolor(var2D,cmap,bounds):
    return shade.spcolor(x2D,y2D,var2D,cmap,bounds)
def sshade_under(var2D,level):
    return shade.sshade_under(x2D,y2D,var2D,level)
def bottom_label(label,lc=None,lw=None,xx=-1700,yy=-2500,area=False):
    return shade.bottom_label(x2D,y2D,label,lc=lc,lw=lw,xx=xx,yy=yy,area=area)
cmap_seq, cmap_smb, cmap_div, cmap_qual, cmap_BuRd, obs_color, sh_color = shade.def_cmaps()

# smb mean std #
# ============ #
def fig1(cmap=cmap_smb):
    # shade annual smb
    scatter_obs_values = False
    mask = lsm2D | (mean2D['mar_ERA-Interim']['smb']<10)
    std_div_mean = ma.array(std2D['mar_ERA-Interim']['smb']/mean2D['mar_ERA-Interim']['smb'],mask=mask)
    # annual smb mean
    bounds=[-20,0,20,50,100,200,400,800,1600]
    fig, gs = shade.set_fig()
    spcolor(mean2D['mar_ERA-Interim']['smb'],cmap,bounds)
    if scatter_obs_values:
        scatter(gridIJ['obs_x'],gridIJ['obs_y'],c=gridIJ['obs_smb'],cmap=cmap,norm=shade.norm_cmap(cmap,bounds),edgecolors='k',\
                s=10,linewidths=0.5)
    scatter(gridIJ['obs_x'],gridIJ['obs_y'],color=obs_color,s=1)
    shade.top_label('Mean of MAR annual SMB (kg m$^{-2}$ yr$^{-1}$)')
    shade.subfig_label('(a)')
    layout()
    shade.plot_cbar(gs,cmap,bounds,extend='both')
    savefig('./fig/article/fig1_MAR_annual_SMB_mean.png',dpi=600)
    # annual smb std
    bounds=[0,5,10,25,50,100,200,400,800]
    fig, gs = shade.set_fig()
    spcolor(std2D['mar_ERA-Interim']['smb'],cmap,bounds)
    scatter(gridIJ['obs_x'],gridIJ['obs_y'],color=obs_color,s=1)
    shade.top_label('Std of MAR annual SMB (kg m$^{-2}$ yr$^{-1}$)')
    shade.subfig_label('(b)')
    layout()
    shade.plot_cbar(gs,cmap,bounds,extend='max')
    savefig('./fig/article/fig1_MAR_annual_SMB_std.png',dpi=600)
    # std/mean
    bounds = list(linspace(0.05,0.2,4))+list(linspace(0.3,0.5,3))+[1.,2.]
    fig, gs = shade.set_fig()
    spcolor(std_div_mean,cmap,bounds)
    scatter(gridIJ['obs_x'],gridIJ['obs_y'],color=obs_color,s=1)
    shade.top_label('(Std/mean) of MAR annual SMB (-)')
    shade.subfig_label('(c)')
    layout()
    shade.plot_cbar(gs,cmap,bounds,extend='max')
    savefig('./fig/article/fig1_MAR_annual_SMB_std-mean.png',dpi=600)
    # MAR bias scatterplot absolute
    bounds=[-1000,-100,-50,-20,-10,0,10,20,50,100,1000]
    bounds=[-1000,-400,-100,-50,-20,-10,0,10,20,50,100,400,1000]
    fig, gs = shade.set_fig()
    layout()
    dsmbIJ = gridIJ['ERA-Interim_smb']-gridIJ['obs_smb']
    scatter(gridIJ['obs_x'],gridIJ['obs_y'],c=dsmbIJ,cmap=cmap_div,norm=shade.norm_cmap(cmap,bounds),edgecolors='k',\
            s=20,linewidths=0.5)
    shade.top_label('(MAR-obs) SMB (kg m$^{-2}$ yr$^{-1}$)')
    shade.subfig_label('(d)')
    shade.plot_cbar(gs,cmap_div,bounds,extend='both')
    savefig('./fig/article/fig1_MAR-vs-obs_SMB_absolute.png',dpi=600)
    # MAR bias scatterplot relative
    bounds=[-2.,-1.,-0.5,-0.3,-0.15,0,0.15,0.3,0.5,1.,2.]
    fig, gs = shade.set_fig()
    layout()
    dsmbIJ_rel = (gridIJ['ERA-Interim_smb']-gridIJ['obs_smb'])/abs(gridIJ['obs_smb'])
    scatter(gridIJ['obs_x'],gridIJ['obs_y'],c=dsmbIJ_rel,cmap=cmap_div,norm=shade.norm_cmap(cmap,bounds),edgecolors='k',\
            s=20,linewidths=0.5)
    shade.top_label('(MAR-obs)/obs SMB (-)')
    shade.subfig_label('(d)')
    shade.plot_cbar(gs,cmap_div,bounds,extend='both')
    savefig('./fig/article/fig1_MAR-vs-obs_SMB_relative.png',dpi=600)
    # values
    # interannual variability
    print round(std_div_mean.mean()*100.)
    print round(ma.array(std_div_mean,mask=mask|(sh2D>2130)).mean()*100.)
    print round(ma.array(std_div_mean,mask=mask|(sh2D<2130)).mean()*100.)
    # bias and rmse
    dsmbIJ.mean()
    figure()
    scatter(gridIJ['obs_smb'],gridIJ['ERA-Interim_smb'],s=10,edgecolors='k',linewidths=0.5)
    plot([0,1200],[0,1200],'k-')
    figure()
    scatter(dsmbIJ,dsmbIJ_rel,s=10,edgecolors='k',linewidths=0.5) # -> 4 patological values
    print round(dsmbIJ.mean()),'=',round(dsmbIJ.mean()/gridIJ['obs_smb'].mean()*100.),'%'
    print round(sqrt((dsmbIJ**2).mean())),'=',round(sqrt((dsmbIJ**2).mean())/gridIJ['obs_smb'].mean()*100.),'%'
    mask=(abs(dsmbIJ)<=400)
    print round(dsmbIJ[mask].mean()),'=',round(dsmbIJ[mask].mean()/gridIJ['obs_smb'][mask].mean()*100.),'%'
    print round(sqrt((dsmbIJ[mask]**2).mean())),'=',\
            round(sqrt((dsmbIJ[mask]**2).mean())/gridIJ['obs_smb'][mask].mean()*100.),'%'

# sublimation #
# =========== #
def shade_var2D(rean,var_name,label='',figlab='',fignum='',figdir='',save=True,extend='both'):
    if var_name in ['smb','snf','rnf']:
        bounds = [-20,0,20,50,100,200,400,800,1600]
        cmap = cmap_smb
    elif var_name in ['sbl','suds','susn']:
        bounds = [-10,-5,0,5,10,20,40,80,160]
        cmap = plt.get_cmap('YlOrBr')
    else:
        bounds = [0,5,10,20,50,100,200,400,800]
        cmap = plt.get_cmap('YlOrBr')
    fig, gs = shade.set_fig()
    layout()
    spcolor(mean2D[rean][var_name],cmap,bounds)
    shade.top_label(label+r' (kg m$^2$ yr$^{-1}$)')
    shade.subfig_label(figlab,dx=230,dy=-650)
    shade.plot_cbar(gs,cmap,bounds,extend=extend)
    if save:
        figname = fignum+'_'+var_name+'_'+rean+'.png'
        savefig('./fig/'+figdir+'/'+figname,dpi=600)

def figS5():
    shade_var2D('mar_ERA-Interim','sbl',label='MAR surface snow sublimation',figlab='(a)',fignum='figS5',figdir='supplementary')
    shade_var2D('rac','sbl',label='RACMO2 total sublimation',figlab='(b)',fignum='figS5',figdir='supplementary')
    shade_var2D('rac','suds',label='RACMO2 drifting snow subl.',figlab='(c)',fignum='figS5',figdir='supplementary')
    shade_var2D('rac','susn',label='RACMO2 surface snow subl.',figlab='(d)',fignum='figS5',figdir='supplementary')

def figS6():
    shade_var2D('mar_ERA-Interim','mlt',label='MAR snow melt',figlab='(a)',fignum='figS6',figdir='supplementary',extend='max')
    shade_var2D('rac','mlt',label='RACMO2 snow melt',figlab='(b)',fignum='figS6',figdir='supplementary',extend='max')


# fig5 and figS3: modelled smb differences #
# ======================================== #

def shade_dsmb2D(rean='RACMO2',typ='abs',racmo_minus_mar=False,\
        figlab='',figname='',figdir='article',var='smb',contours=True):
    '''
    shade smb(rean) - smb(MAR-ERA-Interim)
    typ='abs' or 'rel'
    '''
    if rean == 'RACMO2':
        sublabel = None
        if racmo_minus_mar:
            lab_dvar = 'RACMO2-MAR'
            factor = -1.
        else:
             lab_dvar = 'MAR-RACMO2'
             factor = 1.
        if var == 'smb':
            lab_var = 'SMB'
        elif var == 'snf':
            lab_var = 'snowfall'
        else:
            print 'ERROR: var should be smb or snf'
            return
        if typ=='abs':
            lab_typ=''
            bounds = [-800,-400,-200,-100,-50,-20,0,20,50,100,200,400,800]
            dvar2D = dsmb2D*factor
            if var=='snf':
                dvar2D = dsnf2D*factor
            label = '('+lab_dvar+') annual '+lab_var+' (kg m$^{-2}$ yr$^{-1}$)'
        elif typ=='rel':
            lab_typ='_rel'
            bounds = linspace(-105,105,15)
            dvar2D = dsmb2D_rel*100.*factor
            if var=='snf':
                dvar2D = dsnf2D_rel*100
            label = '('+lab_dvar+')/MAR annual '+lab_var+' (%)'
        figfile =  figname+'_d'+var+'_'+lab_dvar+'.png'
    else:
        dvar2D = mean2D['mar_'+rean][var]-mean2D['mar_ERA-Interim'][var]
        if typ=='abs':
            lab_typ=''
            bounds = [-80,-50,-20,-10,-5,-2,0,2,5,10,20,50,80]
            label = 'MAR('+rean+r')$-$MAR(ERA-Int)'
            sublabel = r'(kg m$^{-2}$ yr$^{-1}$)'
        elif typ=='rel':
            lab_typ='_rel'
            bounds = linspace(-18,18,13)
            label = '(MAR('+rean+r')$-$MAR(ERA-Int))/MAR(ERA-Int)'
            sublabel = '(%)'
            dvar2D = dvar2D/mean2D['mar_ERA-Interim'][var]*100.
        figfile =  figname+'_d'+var+lab_typ+'_MAR-'+rean+'.png'
    fig, gs = shade.set_fig()
    spcolor(dvar2D,cmap_div,bounds)
    layout()
    shade.top_label(label)
    if typ=='rel' and rean != 'RACMO2':
        shade.top_sublabel('(%)',dy=50)
    shade.subfig_label(figlab,dx=230,dy=-650)
    if contours:
        scontour(-1*dsnf2D_rel,[0.15],lw=1.,zorder=100,lc='hotpink')
        scontour(dsnf2D_rel,[0.15],lw=1.,zorder=100,lc='royalblue')
        bottom_label('15% (MAR-RACMO2)/MAR snowfall',lc='royalblue',xx=-1700,yy=-2350)
        bottom_label('15% (RACMO2-MAR)/MAR snowfall',lc='hotpink',xx=-1700,yy=-2650)
    if sublabel is not None:
        shade.top_sublabel(sublabel,dy=50)
    shade.plot_cbar(gs,cmap_div,bounds)
    savefig('./fig/'+figdir+'/'+figfile,dpi=600)

# fig S4: dsmb MAR(ERA-Int) vs MAR(JRA-55) vs MAR(MERRA2)
def figS4():
    shade_dsmb2D(rean='JRA-55',typ='abs',figlab='(a)',figname='figS4',figdir='supplementary',contours=False)
    shade_dsmb2D(rean='MERRA2',typ='abs',figlab='(b)',figname='figS4',figdir='supplementary',contours=False)
    shade_dsmb2D(rean='JRA-55',typ='rel',figlab='(c)',figname='figS4',figdir='supplementary',contours=False)
    shade_dsmb2D(rean='MERRA2',typ='rel',figlab='(d)',figname='figS4',figdir='supplementary',contours=False)

def figS9():
    shade_dsmb2D(rean='RACMO2',typ='abs',var='smb',figlab='(a)',figname='figS9',figdir='supplementary',contours=False)
    shade_dsmb2D(rean='RACMO2',typ='abs',var='snf',figlab='(b)',figname='figS9',figdir='supplementary',contours=False)
    shade_dsmb2D(rean='RACMO2',typ='rel',var='smb',figlab='(c)',figname='figS9',figdir='supplementary',contours=False)
    shade_dsmb2D(rean='RACMO2',typ='rel',var='snf',figlab='(d)',figname='figS9',figdir='supplementary',contours=False)

bounds=[0,10,20,50,100,200,400,800]
# positive snowfall relative difference vs curvature (crests)
def shade_dsnf_vs_curv_crests(figname='',figlab='',cmap=cmap_smb):
    fig, gs = shade.set_fig()
    mask2D = (dsnf2D_rel<0.15)
    spcolor(ma.array(dsnf2D,mask=mask2D),cmap,bounds)
    sshade_under(-curv2D,-0.005) # crests
    layout()
    shade.top_label('(MAR-RACMO2) annual snowfall (kg m$^{-2}$ yr$^{-1}$)')
    shade.top_sublabel('for (MAR-RACMO2)/MAR > 15%')
    shade.subfig_label(figlab,dx=230,dy=-650)
    bottom_label('Curvature > 0.005 (crests)',xx=-1000,yy=-2600,area=True)
    shade.plot_cbar(gs,cmap,bounds,extend='max')
    savefig('./fig/article/'+figname+'_dsnf_crests.png',dpi=600)

# negative snowfall relative difference vs curvature (valleys)
def shade_dsnf_vs_curv_valleys(figname='',figlab='',cmap=cmap_smb):
    fig, gs = shade.set_fig()
    mask2D = (dsnf2D_rel>-0.15)
    spcolor(ma.array(-dsnf2D,mask=mask2D),cmap,bounds)
    sshade_under(curv2D,-0.005) # valleys
    layout()
    shade.top_label('(RACMO2-MAR) annual snowfall (kg m$^{-2}$ yr$^{-1}$)')
    shade.top_sublabel('for (RACMO2-MAR)/MAR > 15%')
    bottom_label('Curvature < -0.005 (valleys)',xx=-1000,yy=-2600,area=True)
    shade.subfig_label(figlab,dx=230,dy=-650)
    shade.plot_cbar(gs,cmap,bounds,extend='max')
    savefig('./fig/article/'+figname+'_dsnf_valleys.png',dpi=600)

# negative snowfall relative difference vs low-level sublimation
def shade_dsnf_vs_curv_subl(figname='',figlab='',cmap=cmap_smb):
    fig, gs = shade.set_fig()
    mask2D = (dsnf2D_rel>-0.15)
    spcolor(ma.array(-dsnf2D,mask=mask2D),cmap,bounds)
    sshade_under(-Gratio2D,-0.15) # sublimation
    layout()
    shade.top_label('(RACMO2-MAR) annual snowfall (kg m$^{-2}$ yr$^{-1}$)')
    shade.top_sublabel('for (RACMO2-MAR)/MAR > 15%')
    bottom_label('Sublimated/Ground snowfall > 15%',xx=-1700,yy=-2450,area=True)
    shade.subfig_label(figlab,dx=230,dy=-650)
    text(-1700,-2500,'(Grazioli et al. 2017)',va='top',ha='left')
    shade.plot_cbar(gs,cmap,bounds,extend='max')
    savefig('./fig/article/'+figname+'_dsnf_rel_Grazioli.png',dpi=600)

def fig5():
    shade_dsmb2D(figname='fig5',figlab='(a)')
    shade_dsnf_vs_curv_crests(figname='fig5',figlab='(b)')
    shade_dsnf_vs_curv_valleys(figname='fig5',figlab='(c)')
    shade_dsnf_vs_curv_subl(figname='fig5',figlab='(d)')

# scatter plots by sectors
# ========================

# define obs uncertainty
def figS3(smb_1=50,smb_2=250):
    # select grid cells with more than 10 observations
    mask = (gridIJ['obs_nb']>10)&(gridIJ['obs_nb']<30)
    xm, ym = gridIJ['obs_x'][mask], gridIJ['obs_y'][mask]
    nb = gridIJ['obs_nb'][mask]
    ostd = gridIJ['obs_smb_std'][mask]
    omm = gridIJ['obs_smb'][mask]
    # divide dataset by smb range
    mask_smb = {'low':(omm<=smb_1),'int':(omm<=smb_2)&(omm>=smb_1),'high':(omm>=smb_2)}
    sect_list = 'low','int','high'
    # linear regression by smb range
    def linreg(mask):
        ss,ii,rr,pp,sterr = scipy.stats.linregress(omm[mask],ostd[mask])
        return ss,ii,rr**2
    ss, ii, r2 = {}, {}, {}
    for sect in sect_list:
        ss[sect], ii[sect], r2[sect] = linreg(mask_smb[sect])
        print sect+':','slope=',round(ss[sect],2),'intercept=',round(ii[sect],1),'r2=',round(r2[sect],2)
    # nicer relationships
    def print_limits(sect):
        print sect+' min:',round(omm[mask_smb[sect]].min(),1),round(omm[mask_smb[sect]].min()*ss[sect]+ii[sect],1)
        print sect+' max:',round(omm[mask_smb[sect]].max(),1),round(omm[mask_smb[sect]].max()*ss[sect]+ii[sect],1)
    print round((5-ii['low'])/ss['low'],1),5
    for sect in sect_list:
        print_limits(sect)
    def slope_intercept(xx,yy):
        slope = (yy[1]-yy[0])/(xx[1]-xx[0])
        intercept = yy[1]-slope*xx[1]
        return slope, intercept
    xlim = np.array([15.,25.,50.,250.,800.])
    ylim = np.array([1.,5.,30.,60.,302.])
    ssp,iip = {},{}
    ssp['low'],iip['low'] = slope_intercept(xlim[1:3],ylim[1:3])
    ssp['int'],iip['int'] = slope_intercept(xlim[2:4],ylim[2:4])
    ssp['high'],iip['high'] = slope_intercept(xlim[3:],ylim[3:])
    for sect in sect_list:
        print sect+':','slope=',round(ssp[sect],2),'intercept=',round(iip[sect],1)
    fw=4.72441 # 12 cm in inches 
    fig = figure(figsize=(fw*1.1,fw))
    clf()
    subplots_adjust(left=0.14,right=0.99,bottom=0.12,top=0.99)
    str_sign = {1:'$+$ ',-1:'$-$ '}
    lba = {}
    def xx(sect):
        imin = {'low':1,'int':2,'high':3}
        return linspace(xlim[imin[sect]],xlim[imin[sect]+1],2)
    col, colp = {}, {}
    for i, sect in enumerate(sect_list):
        col[sect] = plt.get_cmap('viridis')(i/3.)
        colp[sect] = plt.get_cmap('viridis')(i/3.+0.2)
        mask = mask_smb[sect]
        scatter(log(omm[mask]),log(ostd[mask]),c=col[sect],zorder=0,s=20)
        plot(log(xx(sect)),log(maximum(xx(sect)*ss[sect]+ii[sect],5)),c=col[sect],lw=2,ls='--',zorder=10)
        label = 'std = '+str(round(ssp[sect],2))+' mean '+str_sign[sign(iip[sect])]+str(round(abs(iip[sect]),1))+\
                ' (R2='+str(round(r2[sect],2))+')'
        lba[sect], = plot(log(xx(sect)),log(maximum(xx(sect)*ssp[sect]+iip[sect],5)),c=colp[sect],\
                lw=4,zorder=5,label=label)
    plot(log(xlim[:2]),[log(5),log(5)],c=colp['low'],lw=4,zorder=5)
    # plot thresholds
    def plot_thresholds(imin):
        xvar, yvar = np.array([xlim[0],xlim[imin+1]]), np.array([ylim[0],ylim[imin+1]])
        xcst, ycst = np.array([xlim[imin+1],xlim[imin+1]]), np.array([ylim[imin+1],ylim[imin+1]])
        plot(log(xvar),log(ycst),c='grey',lw=1,ls=':')
        plot(log(xcst),log(yvar),c='grey',lw=1,ls=':')
    for imin in range(4):
        plot_thresholds(imin)
    lr, = plot([-10,-10],[-10,-10],c='k',lw=2,ls='--',label='Linear regression')
    lra, = plot([-10,-10],[-10,-10],c='grey',lw=4,label='Adjusted relationship')
    xticklabs = np.array([15,25,50,100,250,500,800])
    plt.xticks(log(xticklabs),xticklabs)
    yticklabs = np.array([1,2,5,10,30,60,150,300])
    plt.yticks(log(yticklabs),yticklabs)
    legend(loc='upper left',handles=[lr,lra,lba['low'],lba['int'],lba['high']])
    axis(log(np.array([15,1000,1,600])))
    ylabel('Std of obs. SMB by grid cell'+r' (kg m$^{-2}$ yr$^{-1}$)')
    xlabel('Mean obs. SMB by grid cell'+r' (kg m$^{-2}$ yr$^{-1}$)')
    # text(,,'(a)',ha='left',va='top',fontsize='large')
    savefig('./fig/supplementary/figS3_scatter.png',dpi=500)
    fig, gs = shade.set_fig()
    layout() 
    for sect in sect_list:
        scatter(xm[mask_smb[sect]],ym[mask_smb[sect]],s=10,c=col[sect],marker='o')
    savefig('./fig/supplementary/figS3_map.png',dpi=600)

def compute_coord(coord,reverse=False):
    if reverse:
        coord_out = coord[-1] - coord
    else:
        coord_out = coord - coord[0]
    return coord_out

sector2D = sectors.get_sector2D(x2D,y2D,sh2D)
def sectorize(sector_name):
    direction = sectors.get_direction(sector_name)
    reverse = sectors.get_reverse(sector_name)
    valIJ = sectors.extract_sort(gridIJ,sector2D[sector_name],direction)
    obsXY = sectors.extract_sort(obs,sector2D[sector_name],direction,selected)
    digitIJ, digitXY = sectors.sector_digitize(valIJ,obsXY,direction)
    sector_bin = sectors.sector_bin(valIJ,digitIJ,rean_list,selected)
    if direction in ['x','y']:
        sector_grid = sectors.bin_to_grid(sector_bin,digitXY)
        coord_grid = compute_coord(obsXY[direction].values,reverse=reverse)
        obs_sh, obs_smb = obsXY['sh'], obsXY['smb']
    elif direction in ['sh']:
        sector_grid = sectors.bin_to_grid(sector_bin,digitIJ)
        coord_grid = valIJ['mar_sh'].values
        obs_sh, obs_smb = valIJ['obs_sh'], valIJ['obs_smb']
    return valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb

def concat_sectors(sector_1,sector_2):
    valIJ_1, obsXY_1, sector_bin_1, sector_grid_1, coord_grid_1, obs_sh_1, obs_smb_1 = sectorize(sector_1)
    valIJ_2, obsXY_2, sector_bin_2, sector_grid_2, coord_grid_2, obs_sh_2, obs_smb_2 = sectorize(sector_2)
    valIJ = pd.concat([valIJ_1,valIJ_2])
    obsXY = pd.concat([obsXY_1,obsXY_2])
    sector_bin = {var_name:pd.concat([sector_bin_1[var_name],sector_bin_2[var_name]]) \
            for var_name in list(sector_bin_2)}
    sector_grid = {var_name:np.concatenate([sector_grid_1[var_name],sector_grid_2[var_name]]) \
            for var_name in list(sector_grid_2)}
    dist_sectors = sqrt((obsXY_1['x'][-1]-obsXY_2['x'][0])**2.+(obsXY_1['y'][-1]-obsXY_2['y'][0])**2.)
    reverse = sectors.get_reverse(sector_1)
    if reverse:
        coord2 = coord_grid_2
        coord1 = coord_grid_1 + coord2[0] + dist_sectors
    else:
        coord1 = coord_grid_1
        coord2 = coord_grid_2 + coord1[-1] + dist_sectors
    coord_grid = np.concatenate([coord1,coord2])
    obs_sh = pd.concat([obs_sh_1,obs_sh_2])
    obs_smb = pd.concat([obs_smb_1,obs_smb_2])
    return valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb

sector = {}
for sector_name in ('MBL-Ross-margin','MBL-Ross-inland','VL','DDU-DC','ZGN-DA','DML-margin','DML'):
    sector[sector_name] = sectorize(sector_name)
sector['LD-WL'] = concat_sectors('LD-y','LD-x')
sector['MAW-LG'] = concat_sectors('MAW-LG-y','MAW-LG-x')
sector['SYW-DF'] = concat_sectors('SYW-DF-x','SYW-DF-y')

sector_list = 'MBL-Ross-margin','MBL-Ross-inland','VL','DDU-DC','LD-WL','ZGN-DA','MAW-LG','SYW-DF','DML-margin','DML'
# Table 1
def table1():
    print
    ref_list={1:'Clausen:1979vz',2:'Venteris:1998iy',3:'Vaughan:1999bg',4:'Magand:2007jm',\
            5:'Frezzotti:2004jl',6:'Frezzotti:2007fg',7:'Pettre:1986ji',8:'Agosta:2012kw',\
            9:'Verfaillie:2012ig',10:'Goodwin:1988ve',11:'Ding:2011dh',12:'Wang:2016iv',\
            13:'Higham:1997tw',14:'Wang:2015ke',15:'GLACIOCLIM-BELARE',16:'Picciotto:1968gk',\
            17:'MosleyThompson:1995bl',18:'MosleyThompson:1999ir',19:'Anschutz:2011dl'}
    ref_sector={'MBL-Ross-margin':'[1,2,3]','MBL-Ross-inland':'[1,2,3]','VL':'[4,5,6]','DDU-DC':'[4,7,8,9]',\
        'LD-WL':'[10]','ZGN-DA':'[11,12]','MAW-LG':'[13]','SYW-DF':'[14]','DML-margin':'[15]',\
        'DML':'[16,17,18,19]'}
    for sector_name in sector_list:
        valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb = sector[sector_name]
        print sectors.get_longname(sector_name),'&',sectors.get_type(sector_name),'&',obsXY.shape[0],\
        '&',valIJ.shape[0],'&',str(int(valIJ['yearstart'].min()))+'--'+str(int(valIJ['yearend'].max())),\
        '&',str(int(obsXY['sh'].min()))+'--'+str(int(obsXY['sh'].max())),'&',ref_sector[sector_name]+' \\\\'
    references = ''
    for number in range(1,20):
        if number==15:
            references = references+'['+str(number)+'] '+ref_list[number]+', '
        else:
            references = references+'['+str(number)+'] \\citet{'+ref_list[number]+'}, '
    print references
def table1_refs():
    print
    for sector_name in sector_list:
        print sector_name
        valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb = sector[sector_name]
        print obsXY['ref'].groupby(obsXY['ref']).count()

def nint(number):
    return int(round(number))

def mb(rean,var_name,mask):
    tmb = annual_mass_balance(mean3D[rean][var_name],mask)
    return '$'+str(nint(tmb.mean()))+' \\pm '+str(nint(tmb.std()))+'$'

# Table 2
def table2():
    print
    print '\\tophline'
    print 'Basin & Area (10$^6$ km$^2$) & Component (\\unit{Gt} \\unit{yr^{-1}}) & MAR(ERA-Interim) & RACMO2(ERA-Interim) \\\\'
    print '\\middlehline'
    print 'TIS w/o Peninsula &$',total_area(mask_tis_nopis),\
            '$& SMB &',mb('mar_ERA-Interim','smb',mask_tis_nopis),'&',mb('rac','smb',mask_tis_nopis),'\\\\'
    print ' & & Snowfall &',mb('mar_ERA-Interim','snf',mask_tis_nopis),'&',mb('rac','snf',mask_tis_nopis),'\\\\'
    print ' & & Rainfall &',mb('mar_ERA-Interim','rnf',mask_tis_nopis),'&',mb('rac','rnf',mask_tis_nopis),'\\\\'
    print ' & & Snow surface sublimation &',mb('mar_ERA-Interim','sbl',mask_tis_nopis),'&',mb('rac','susn',mask_tis_nopis),'\\\\'
    print ' & & Drifting snow sublimation & -- &',mb('rac','suds',mask_tis_nopis),'\\\\'
    print ' & & Erosion-deposition & -- &',mb('rac','erds',mask_tis_nopis),'\\\\'
    print ' & & Melt &',mb('mar_ERA-Interim','mlt',mask_tis_nopis),'&',mb('rac','mlt',mask_tis_nopis),'\\\\'
    print ' & & Run-off &',mb('mar_ERA-Interim','rof',mask_tis_nopis),'&',mb('rac','rof',mask_tis_nopis),'\\\\'
    print 'TIS &$',total_area(mask_tis),'$& SMB &',mb('mar_ERA-Interim','smb',mask_tis),'&',mb('rac','smb',mask_tis),'\\\\'
    print '\\middlehline'
    print 'GIS &$',total_area(mask_gis),'$& SMB &',mb('mar_ERA-Interim','smb',mask_gis),'&',mb('rac','smb',mask_gis),'\\\\'
    print 'GIS w/o Peninsula &$',total_area(mask_gis_nopis),'$& SMB &',mb('mar_ERA-Interim','smb',mask_gis_nopis),'&',mb('rac','smb',mask_gis_nopis),'\\\\'
    print 'EAIS &$',total_area(mask_eais),'$& SMB &',mb('mar_ERA-Interim','smb',mask_eais),'&',mb('rac','smb',mask_eais),'\\\\'
    print 'WAIS &$',total_area(mask_wais),'$& SMB &',mb('mar_ERA-Interim','smb',mask_wais),'&',mb('rac','smb',mask_wais),'\\\\'
    print 'Islands &$',total_area(mask_islands),'$& SMB &',mb('mar_ERA-Interim','smb',mask_islands),'&',mb('rac','smb',mask_islands),'\\\\'
    print 'Peninsula &$',total_area(mask_pis),'$& SMB &',mb('mar_ERA-Interim','smb',mask_pis),'&',mb('rac','smb',mask_pis),'\\\\'
    print '\\bottomhline'

# plots

# time series
def dtyear(year_min,year_max):
    return np.array([pd.datetime(year,6,15) for year in range(year_min,year_max+1)])
dtyr = {'rac':dtyear(1979,2015),'mar_ERA-Interim':dtyear(1979,2015),'mar_JRA-55':dtyear(1979,2014),'mar_MERRA2':dtyear(1980,2015)}
lcc = {'rac':cmap_BuRd(0.8),'mar_ERA-Interim':cmap_BuRd(0.2),'mar_JRA-55':cmap_seq(0.7),'mar_MERRA2':cmap_seq(0.5)}
lab = {'rac':'RACMO2(ERA-Interim)','mar_ERA-Interim':'MAR(ERA-Interim)','mar_JRA-55':'MAR(JRA-55)','mar_MERRA2':'MAR(MERRA2)'}
lww = {'rac':2.5,'mar_ERA-Interim':2.5,'mar_JRA-55':1,'mar_MERRA2':1}
lab_var = {'snf':'Snowfall','rnf':'Rainfall','sbl':'Sublimation','rof':'Runoff','mlt':'Snowmelt','smb':'SMB'}
zo = {'rac':2,'mar_ERA-Interim':3,'mar_JRA-55':1,'mar_MERRA2':0}
def plot_annual(var_name,mask,figname='',figlab=''):
    fw = 5.5 # 14 cm
    fh = fw/2.
    fig=figure(figsize=(fw,fh))
    clf()
    subplots_adjust(left=0.14,right=0.98,bottom=0.26,top=0.96)
    for rean in 'rac','mar_ERA-Interim','mar_JRA-55','mar_MERRA2':
        tmb = annual_mass_balance(mean3D[rean][var_name],mask)
        plot_date(dtyr[rean],tmb,ls='-',marker=None,color=lcc[rean],lw=lww[rean],label=lab[rean],zorder=zo[rean])
    if var_name == 'sbl':
        rean = 'rac'
        suds = annual_mass_balance(mean3D[rean]['suds'],mask)
        susn = annual_mass_balance(mean3D[rean]['susn'],mask)
        lds, = plot_date(dtyr[rean],suds,ls='-.',marker=None,color=lcc[rean],lw=1.5,zorder=zo[rean],label='RACMO2 drifting snow sublimation')
        lsn, = plot_date(dtyr[rean],susn,ls='--',marker=None,color=lcc[rean],lw=1.5,zorder=zo[rean],label='RACMO2 surface snow sublimation')
        legend(loc='upper left',handles=[lds,lsn])
    ylabel(lab_var[var_name]+' (Gt yr$^{-1}$)',fontsize='large')
    xlabel('Years',fontsize='large')
    if var_name in ['smb','snf']:
        ylim(1650,2650)
    else:
        ylim(0,265)
    if var_name == 'snf':
        legend(mode='expand',ncol=2)
    text(-0.11,1.04,figlab,ha='right',va='top',transform=gca().transAxes,fontsize='large')
    gca().xaxis.set_major_locator(mdates.YearLocator(2, month=6, day=15))
    xticks(rotation=80)
    savefig('./fig/supplementary/'+figname+'_annual_'+var_name+'.png',dpi=500)
def figS7():
    plot_annual('smb',mask_tis_nopis,figname='figS7',figlab='(a)')
    plot_annual('snf',mask_tis_nopis,figname='figS7',figlab='(b)')
    plot_annual('sbl',mask_tis_nopis,figname='figS7',figlab='(c)')
    plot_annual('mlt',mask_tis_nopis,figname='figS7',figlab='(d)')

# dsmb
def remove_spines(axis):
    axis.spines['right'].set_color('none')
    axis.spines['top'].set_color('none')
    axis.spines['bottom'].set_color('none')
    axis.xaxis.set_ticks([])

smb_max_dict={'LD-x':900.,'LD-y':900.,'SYW-DF-x':420,'SYW-DF-y':420,'DDU-DC':600,'DML-margin':630,\
        'MBL-Ross-inland':280,'MBL-Ross-margin':350,'SYW-DF':420}
def plot_sector_smb(sector_name,verify=False,show_slope=False,\
        ylab=True,min_subl=15,ymax=0,figname=''):
    '''
    plot a cross sector:
    for the sector 'sector_name', observation and model results are averaged
    along the direction 'direction' 'x' or 'y'
    '''
    direction = sectors.get_direction(sector_name)
    valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb = sector[sector_name]
    # plot cross-sector
    # ==================
    lgray = plt.get_cmap('gray')(0.9)
    dgray = plt.get_cmap('gray')(0.2)
    dxy = coord_grid.max()-coord_grid.min()
    if direction == 'sh':
        dxy = dxy/2.
    left, right, bottom, top, hspace = 0.65, 0.12, 0.13, 0.35, 0.4 # in inches
    w = max(3.5*(dxy/1000.)+left+right,1.5)
    h = 3.5
    fig=figure(figsize=(w,h))
    gs = gridspec.GridSpec(4, 1, height_ratios=[0.5,0.5,0.5,2.5])
    xmin, xmax = coord_grid.min()-10, coord_grid.max()+10
    gs.update(left=left/w, right=(w-right)/w, bottom=bottom, top=(h-top)/h, hspace=hspace)
    clf()
    # year range
    ax1 = subplot(gs[0])
    ax1.fill_between(coord_grid,sector_grid['yearstart']*0+1950,sector_grid['yearend']*0+2015,color=lgray)
    ax1.fill_between(coord_grid,sector_grid['yearstart'],sector_grid['yearend'],color='gray')
    ax1.plot(coord_grid,sector_grid['yearstart']*0+1979,ls='--',color=dgray,lw=1)
    ax1.set_ylim(bottom=1950,top=2015)
    ax1.set_xlim(left=xmin,right=xmax)
    if ylab and (w>1.5):
        ax1.text(0.,0.95,'Year range',ha='left',va='bottom',transform=ax1.transAxes)
    remove_spines(ax1)
    # nb of data
    ax2 = subplot(gs[1])
    ax2.fill_between(coord_grid,sector_grid['yearstart']*0,sector_grid['yearend']*0+24,color=lgray)
    ax2.fill_between(coord_grid,sector_grid['obs_nb']*0,sector_grid['obs_nb'],color='gray')
    ax2.plot(coord_grid,sector_grid['obs_nb']*0+10,ls='--',color=dgray,lw=1)
    ax2.set_ylim(bottom=0.,top=24)
    ax2.set_xlim(left=xmin,right=xmax)
    if ylab and (w>1.5):
        ax2.text(0.,0.95,'Nb. of obs. by grid cell',ha='left',va='bottom',transform=ax2.transAxes)
    remove_spines(ax2)
    # sh
    ax3 = subplot(gs[2])
    cla()
    shmax = obs_sh.max()+200.
    ax3.fill_between(coord_grid,obs_sh*0,obs_sh*0+shmax,color=lgray,zorder=1)
    ax3.plot(coord_grid,obs_sh,c=dgray,lw=1,zorder=3) # ,marker='o',ms=2,mew=1,mfc='lightgrey'
    ax3.fill_between(coord_grid,obs_sh,sector_grid['mar_sh'],color=cmap_BuRd(0.25),zorder=1)
    ax3.plot(coord_grid,sector_grid['mar_sh'],color=cmap_BuRd(0.25),lw=1,zorder=2)
    ax3.plot(coord_grid,sector_grid['mar_sh']*0+2000.,ls='--',color=dgray,lw=1)
    ax3.set_ylim(bottom=0.,top=shmax)
    ax3.set_xlim(left=xmin,right=xmax)
    if ylab and (w>1.5):
        ax3.text(0.,0.95,'Elevation (m)',ha='left',va='bottom',transform=ax3.transAxes)
    remove_spines(ax3)
    # smb
    ax4 = subplot(gs[3])
    cla()
    ax4.xaxis.set_ticks_position('both')
    more10obs, lesS9obs = (sector_grid['obs_nb']>=10), (sector_grid['obs_nb']<10)
    def line_scatter(val_grid,c='k',zorder=8,s=6):
        ax4.plot(coord_grid,val_grid,c=c,ls='-',lw=1,zorder=zorder)
        ax4.scatter(coord_grid[more10obs],val_grid[more10obs],s=s,edgecolor=c,facecolor=c,zorder=zorder+1)
        ax4.scatter(coord_grid[lesS9obs],val_grid[lesS9obs],s=s,edgecolor=c,facecolor='w',\
                linewidth=1,zorder=zorder+1)
    line_scatter(sector_grid['obs_smb'])
    ax4.fill_between(coord_grid,sector_grid['obs_smb_inf'],sector_grid['obs_smb_sup'],alpha=0.8,color='lightgrey')
    line_scatter(sector_grid['RACMO2_smb'],c=cmap_BuRd(0.8),s=3,zorder=6)
    line_scatter(sector_grid['ERA-Interim_smb'],c=cmap_BuRd(0.2),s=3,zorder=4)
    if sector_name in list(smb_max_dict):
        ymax = smb_max_dict[sector_name]
        ax4.set_ylim(top=ymax)
    mask_subl = where((sector_grid['subl']*100).astype(int)<=min_subl,1,0)
    ax4.fill_between(coord_grid,ma.array(mask_subl*0+ymax/20,mask=mask_subl),\
            ma.array(mask_subl*0+ymax/10.,mask=mask_subl),color='m',zorder=0)
    rean_MAR_list = [rean for rean in rean_list if rean not in ['ERA-Interim','RACMO2']]
    for i,rean in enumerate(rean_MAR_list):
        ipp = 0.5+i/((len(rean_MAR_list)*2.5))
        ax4.plot(coord_grid,sector_grid[rean+'_smb'],lw=1,color=cmap_seq(ipp))#,alpha=0.8)
    ax4.fill_between(coord_grid,sector_grid['smb_MAR_min'],sector_grid['smb_MAR_max'],alpha=0.5,color=cmap_seq(0.6),zorder=1)
    ax4.set_xlim(left=xmin,right=xmax)
    ax4.set_ylim(bottom=-5)
    if direction == 'sh':
        xlabel('Elevation (m)')
    else:
        xlabel('Distance along transect (km)')
    if ylab:
        ylabel('SMB (kg m$^{-2}$ yr$^{-1}$)')
    fig.suptitle(sectors.get_longname(sector_name))
    savefig('./fig/article/'+figname+'_smb_'+sector_name+'.png',dpi=600)

def plot_sector_dsmb(sector_name,direction='xy',verify=False,\
        show_slope=False,ylab=True,figlab='',figname=''):
    '''
    plot a cross sector:
    for the sector 'sector_name', observation and model results are averaged
    along the direction 'direction' 'x' or 'y'
    '''
    valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb = sector[sector_name]
    # plot cross-sector
    # ==================
    lgray = plt.get_cmap('gray')(0.9)
    dgray = plt.get_cmap('gray')(0.2)
    dxy = coord_grid.max()-coord_grid.min()
    dsh = sector_bin['mar_sh'].max()-sector_bin['mar_sh'].min()
    left, right, bottom, top, hspace = 0.8, 0.15, 0.15, 0.3, 0.35 # in inches
    w = max(3.5*(dxy/1000.)+left+right,1.5)
    h = 4.
    fig=figure(figsize=(w,h))
    gs = gridspec.GridSpec(3, 1, height_ratios=[0.5,1,1])
    gs.update(left=left/w, right=(w-right)/w, bottom=bottom, top=(h-top)/h, hspace=hspace)
    clf()
    # MAR 10 m wind speed
    ax1 = subplot(gs[0])
    cla()
    ax1.text(-230,15.5,figlab,fontsize='large')
    ax1.plot(coord_grid,sector_grid['ws'],color=cmap_BuRd(0.18),lw=1.2,zorder=4)
    ax1.plot([coord_grid.min(),coord_grid.max()],[9,9],c='grey',ls='--',zorder=0,lw=1)
    ax1.plot([coord_grid.min(),coord_grid.max()],[5,5],c='grey',ls='--',zorder=0,lw=1)
    ax1.set_xlim(left=coord_grid.min(),right=coord_grid.max())
    ax1.set_ylim(bottom=4,top=13)
    yticks([5,9])
    ylabel('10 m wind \n speed (m s$^{-1}$)')
    # curvature
    ax1 = subplot(gs[1])
    cla()
    curv_lim = 0.015
    ax1.plot(coord_grid,sector_grid['rac_curv'],color=cmap_BuRd(0.8),lw=1,zorder=4)
    ax1.plot(coord_grid,sector_grid['curv'],color=cmap_BuRd(0.18),lw=1,zorder=3)
    ax1.fill_between(coord_grid,sector_grid['curv']*0,np.maximum(sector_grid['curv'],0),color=cmap_div(0.6))
    ax1.fill_between(coord_grid,sector_grid['curv']*0,np.minimum(sector_grid['curv'],0),color=cmap_div(0.4))
    ax1.plot(coord_grid,sector_grid['curv']*0,color='k',lw=1)
    ax1.set_ylim(bottom=-curv_lim,top=curv_lim)
    ax1.set_xlim(left=coord_grid.min(),right=coord_grid.max())
    if ylab:
        ax1.text(-115,0.002,'Curvature \n (10$^{-6}$ m$^{-1}$)',rotation=90,ha='right',va='center')
    # dsmb
    ax2 = subplot(gs[2])
    cla()
    dsmb_mar = sector_grid['ERA-Interim_smb']-sector_grid['obs_smb']
    dsmb_rac = sector_grid['RACMO2_smb']-sector_grid['obs_smb']
    if sector_name == 'LD-WL':
        dsmb_mar = dsmb_mar - 30
        ax2.text(0.01,0.95,'MAR: -30',ha='left',va='top',transform=ax2.transAxes)
    dsmb_lim = 60
    ax2.plot(coord_grid,dsmb_rac,color=cmap_BuRd(0.8),lw=1,zorder=4)
    ax2.plot(coord_grid,dsmb_mar,color=cmap_BuRd(0.18),lw=1,zorder=3)
    ax2.fill_between(coord_grid,sector_grid['curv']*0,np.maximum(dsmb_mar,0),color=cmap_div(0.6),lw=1)
    ax2.fill_between(coord_grid,sector_grid['curv']*0,np.minimum(dsmb_mar,0),color=cmap_div(0.4),lw=1)
    ax2.plot(coord_grid,dsmb_mar*0,color='k',lw=1)
    if ylab:
        ylabel('$\Delta$SMB (kg m$^{-2}$ yr$^{-1}$)')
    if w>1.5:
        xlabel('Distance along transect (km)')
    else:
        xlabel('Distance')
    ax2.set_ylim(bottom=-dsmb_lim,top=dsmb_lim)
    ax2.set_xlim(left=coord_grid.min(),right=coord_grid.max())
    fig.suptitle(sectors.get_longname(sector_name))
    savefig('./fig/article/'+figname+'_dsmb_'+sector_name+'.png',dpi=600)

def fig2():
    for sector_name in list(sector):
        plot_sector_smb(sector_name,figname='fig2')

def fig3():
    figlab = {'LD-WL':'(a)','ZGN-DA':'(b)','MAW-LG':'(c)','SYW-DF':'(d)'}
    for sector_name in ('LD-WL','ZGN-DA','MAW-LG','SYW-DF'):
        plot_sector_dsmb(sector_name,figname='fig3',figlab=figlab[sector_name])

# erosion-deposition quantification #
# ================================= #

def shift_data(data_in,delta):
    if delta < 0:
        data_shift = data_in[:delta].values
    elif delta > 0:
        data_shift = data_in[delta:].values
    elif delta == 0:
        data_shift = data_in.values
    else:
        print 'Error in shift, delta must be an integer'
    return data_shift

def cut_data(data_in,delta):
    if delta < 0:
        data_cut = data_in[abs(delta):].values
    elif delta > 0:
        data_cut = data_in[:-delta].values
    elif delta == 0:
        data_cut = data_in.values
    else:
        print 'Error in cut, delta must be an integer'
    return data_cut

def init_df(index,columns):
    empty_data = [[np.nan for var in columns] for name in index]
    return pd.DataFrame(empty_data,columns=columns,index=index)

def fact(ws,vmin,vmax):
    fact = (ws-vmin)/(vmax-vmin)
    fact = minimum(fact,1.)
    fact = maximum(fact,0.)
    return fact

def erds(ws,vmin=0,vmax=1,alpha=3700):
    erds = curv2D*alpha
    erds = minimum(erds,50)
    erds = maximum(erds,-50)
    return erds*fact(ws,vmin,vmax)

def dsmb_vs_curv(rean='ERA-Interim',fill=False):
    color_set = [cmap_seq((i+1)/3.) for i in range(3)]
    transect_list = ('LD-WL','MAW-LG','ZGN-DA','SYW-DF') # 'DDU-DC',
    # extract shifted data
    linreg, dsmb_d, curv_d, ws_d = {}, {}, {}, {}
    delta_range = linspace(-2,2,5).astype(int)
    reg_names = ['slope','intercept','r-value','p-value','stderr']
    for sector_name in transect_list:
        linreg[sector_name] = init_df(reg_names,delta_range)
        curv_d[sector_name], dsmb_d[sector_name], ws_d[sector_name] = {}, {}, {}
        valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb = sector[sector_name]
        dsmb_bin = sector_bin[rean+'_smb'] - sector_bin['obs_smb']
        ws_bin, curv_bin = sector_bin['ws'], sector_bin['curv']
        if rean == 'RACMO2':
            curv_bin = sector_bin['rac_curv']
        if (sector_name=='LD-WL')&(rean=='ERA-Interim'):
            dsmb_bin = dsmb_bin-30
        mask = (abs(dsmb_bin)<40)&(abs(curv_bin)<0.015)
        curv_bin, ws_bin, dsmb_bin = curv_bin[mask], ws_bin[mask], dsmb_bin[mask]
        for delta in delta_range:
            curv_shift, dsmb_cut, ws_cut = shift_data(curv_bin,delta), cut_data(dsmb_bin,delta), cut_data(ws_bin,delta)
            curv_d[sector_name][delta], dsmb_d[sector_name][delta], ws_d[sector_name][delta] = curv_shift, dsmb_cut, ws_cut
            linreg[sector_name][delta] = scipy.stats.linregress(curv_shift[ws_cut>=7],dsmb_cut[ws_cut>=7])
    # plot shifted data
    def set_smallfig():
        fw=3.15/1.5 # 8 cm in inches 
        fh=fw*1.5
        fig = figure(figsize=(fh,fw))
        clf()
    def set_smallfig_squared():
        fw=3.15 # 8 cm in inches 
        fh=fw*1.
        fig = figure(figsize=(fh,fw))
        clf()
    # find shift
    def figS8_shift():
        for sector_name in transect_list:
            set_smallfig()
            width = 0.5
            rval, pval = linreg[sector_name].loc['r-value'], linreg[sector_name].loc['p-value']
            bar(delta_range,rval,width,color=color_set[0])
            mask = (pval.values <= 0.05)&(rval.values>0.)
            if mask.sum()>0:
                bar(delta_range[mask],rval[mask],width,color=color_set[1],edgecolor='k')
            plot([-2.5,2.5],[0,0],'k-',lw=0.5)
            axis([-2.5,2.5,-0.7,0.7])
            xticks(delta_range)
            xlabel('Shift')
            ylabel('R')
            title(sectors.get_longname(sector_name))
            subplots_adjust(left=0.21,right=0.98,bottom=0.21,top=0.87)
            savefig('./fig/supplementary/figS8_R_dsmb_vs_curv_'+sector_name+'.png',dpi=600)
    figS8_shift()
    # global scatterplot -> slope
    delta_dict={'DDU-DC':-1,'LD-WL':-1,'MAW-LG':-1,'ZGN-DA':1,'SYW-DF':2}
    col = np.concatenate([zeros_like(curv_d[sector_name][delta_dict[sector_name]])+sector_idx[sector_name] \
            for sector_name in transect_list])
    x_lim, y_lim = np.array([-0.015,0.015]),np.array([-55,55])
    def fig4_scatter(vmin=0,vmax=9):
        xx = np.concatenate([curv_d[sector_name][delta_dict[sector_name]] for sector_name in transect_list])
        yy = np.concatenate([dsmb_d[sector_name][delta_dict[sector_name]] for sector_name in transect_list])
        ww = np.concatenate([ws_d[sector_name][delta_dict[sector_name]] for sector_name in transect_list])
        mask_ws = (ww<7.)
        mask_out = (xx<-0.005)&(yy>10)
        mask = invert(mask_ws|mask_out) # remove low wind speed and 2 anomalous data
        x_minmax = np.array([xx.min(), xx.max()])
        lreg = scipy.stats.linregress(xx[mask],yy[mask])
        sl0 = (xx[mask]*yy[mask]).sum()/(xx[mask]*xx[mask]).sum()
        r2 = ((xx[mask]*sl0-yy[mask].mean())**2.).sum()/((yy[mask]-yy[mask].mean())**2.).sum()
        t = scipy.stats.t.ppf(0.975,xx[mask].shape[0]-2) # t-value for 95% confidence interval
        sl_err = t*lreg[4]
        print 'all:', round(lreg[0]), round(lreg[1]), round(sl0), round(sl_err)
        fw=4.72441 # 12 cm in inches
        fig = figure(figsize=(fw,fw))
        clf()
        scatter(xx[mask],yy[mask],c=col[mask],s=30,vmin=vmin,vmax=vmax,cmap=cmap_qual)
        scatter(xx[mask_out],yy[mask_out],c=col[mask_out],edgecolors='k',\
                s=30,vmin=vmin,vmax=vmax,linewidths=1.,cmap=cmap_qual)
        scatter(xx[mask_ws],yy[mask_ws],c=col[mask_ws],edgecolors='k',\
                s=30,marker='s',linewidths=1.,vmin=vmin,vmax=vmax,cmap=cmap_qual)
        if fill:
            fill_between(x_minmax,x_minmax*(sl0-sl_err),x_minmax*(sl0+sl_err),color='deeppink',linestyles='None',alpha=0.2,zorder=0)
        label = r'$\alpha = 3700$ ($10^6$ kg m$^{-1}$ yr$^{-1}$)'
        plot(x_minmax,x_minmax*3700,c='deeppink',ls='--',lw=3,label=label)
        plot(x_lim,[0,0],ls='-',lw=0.5,color='gray',zorder=0)
        plot([0,0],y_lim,ls='-',lw=0.5,color='gray',zorder=0)
        axis([x_lim[0],x_lim[1],y_lim[0],y_lim[1]])
        xlabel(r'Curvature ($10^{-6}$ m$^{-1}$)',fontsize='large')
        ylabel(r'$\Delta$SMB (kg m$^{-2}$ yr$^{-1}$)',fontsize='large')
        text(-0.014,48,r'R$^2=$'+str(round(r2,2)),fontsize='large')
        text(-0.02,58,'(a)',fontsize='large')
        legend(bbox_to_anchor=(-0.02,0.97), loc='lower left',fontsize='large')
        subplots_adjust(left=0.15,right=0.95,bottom=0.12,top=0.93)
        savefig('./fig/article/fig4_scatter_dsmb_vs_curv_all.png',dpi=550)
    fig4_scatter()
    # individual scatterplots
    def figS8_scatter(vmin=0,vmax=9):
        for sector_name in transect_list:
            set_smallfig_squared()
            delta = delta_dict[sector_name]
            lreg = linreg[sector_name][delta]
            cc, ss, ww = curv_d[sector_name][delta],dsmb_d[sector_name][delta],ws_d[sector_name][delta]
            col = cc*0. + sector_idx[sector_name]
            mask_ws = (ww<7.)
            mask_out = (cc<-0.005)&(ss>10)
            mask = invert(mask_ws|mask_out) # remove low wind speed and 2 anomalous data
            bounds=linspace(5,9,5)
            scatter(cc[mask],ss[mask],c=col[mask],s=20,vmin=vmin,vmax=vmax,cmap=cmap_qual)
            scatter(cc[mask_out],ss[mask_out],c=col[mask_out],edgecolors='k',\
                    s=20,vmin=0,vmax=9,linewidths=1.,cmap=cmap_qual)
            scatter(cc[mask_ws],ss[mask_ws],c=col[mask_ws],edgecolors='k',\
                    s=20,marker='s',linewidths=1,vmin=0,vmax=9,cmap=cmap_qual)
            x_minmax = np.array([cc.min(), cc.max()])
            slope0 = (cc*ss).sum()/(cc*cc).sum()
            plot(x_minmax,x_minmax*3700,c='deeppink',ls='--',lw=2)
            print sector_name, round(lreg['slope']), round(lreg['intercept']), round(slope0)
            plot(x_lim,[0,0],ls='-',lw=0.5,color='gray',zorder=0)
            plot([0,0],y_lim,ls='-',lw=0.5,color='gray',zorder=0)
            xlabel(r'Curvature ($10^{-6}$ m$^{-1}$)')
            ylabel('$\Delta$SMB (kg m$^{-2}$ yr$^{-1}$)')
            axis([x_lim[0],x_lim[1],y_lim[0],y_lim[1]])
            subplots_adjust(left=0.22,right=0.97,bottom=0.15,top=0.9)
            title(sectors.get_longname(sector_name))
            text(-0.014,47,'Shift='+str(delta))
            savefig('./fig/supplementary/figS8_scatter_dsmb_vs_curv_'+sector_name+'.png',dpi=600)
    figS8_scatter()

# map wind speed 
def shade_ws():
    linspace(4,12,9)
    fig, gs = shade.set_fig()
    spcolor(ws2D,cmap_smb,wsbounds)
    scatter(obs['x'],obs['y'],s=1,c=obs_color,marker='o')
    layout()
    shade.top_label('MAR annual wind speed (m s$^{-1}$)')
    shade.plot_cbar(gs,cmap_smb,wsbounds,extend='max')
    savefig('./fig/article/ws_MAR.png',dpi=600)

def shade_erosion_racmo(scale=False,figname='',figlab=''):
    fig, gs = shade.set_fig()
    bounds = linspace(-14,14,15)
    lab=''
    if scale:
        bounds = linspace(-42,42,15)
        lab='_scale'
    pcolor(x2D,y2D,mean2D['rac']['erds'],cmap=cmap_div,norm=shade.norm_cmap(cmap_div,bounds))
    scatter(obs['x'],obs['y'],s=1,c=obs_color,marker='o')
    layout()
    shade.top_label('RACMO2 annual erosion (kg m$^{-2}$ yr$^{-1}$)')
    shade.subfig_label(figlab,dx=230,dy=-650)
    shade.plot_cbar(gs,cmap_div,bounds,extend='both')
    savefig('./fig/article/'+figname+'_erds_RACMO2'+lab+'.png',dpi=600)

def shade_erosion(vmin=5,vmax=9,figname='',figlab=''):
    bounds = linspace(-42,42,15)
    fig, gs = shade.set_fig()
    spcolor(erds(ws2D,vmin=vmin,vmax=vmax),cmap_div,bounds)
    scatter(obs['x'],obs['y'],s=1,c=obs_color,marker='o',zorder=200)
    shade.top_label(r'Erosion-deposition'+r'$\approx{}\alpha$(ws) $\times$'+r' curvature')
    text(0,2500,r'(kg m$^{-2}$ yr$^{-1}$)',va='top',ha='center',fontsize='large')
    shade.subfig_label(figlab,dx=230,dy=-650)
    label = 'vmin:'+str(int(vmin))+' vmax:'+str(int(vmax))
    figlabel = 'vmin'+str(int(vmin))+'_vmax'+str(int(vmax))
    layout()
    shade.plot_cbar(gs,cmap_div,bounds,extend='both')
    savefig('./fig/article/'+figname+'_erds_curvature_'+figlabel+'.png',dpi=600)

def fig4():
    dsmb_vs_curv()
    shade_erosion(vmin=5,vmax=9,figname='fig4',figlab='(b)')
    shade_erosion_racmo(figname='fig4',figlab='(c)')

def figS8():
    dsmb_vs_curv()

def total_erds(erds,mask):
    er = total_mass_balance(ma.array(erds,mask=erds<0),mask)
    dep = total_mass_balance(ma.array(erds,mask=erds>0),mask)*(-1)
    net = er - dep
    return {'er':int(round(er)),'dep':int(round(dep)),'net':int(round(net))}

def tableS2():
    erds_tot = {}
    mask_dict = {'tis':mask_tis_nopis,'gis':mask_gis_nopis}
    for mask_name in list(mask_dict):
        for vmin in [5,6]:
            for vmax in [8,9]:
                for alpha in [2700,3700,4700]:
                    label = mask_name+'('+str(alpha)+','+str(vmin)+','+str(vmax)+')'
                    erds_estimate = erds(ws2D,vmin=vmin,vmax=vmax,alpha=alpha)
                    erds_tot[label] = total_erds(erds_estimate,mask_dict[mask_name])
    erds_tot['tis_RACMO2'] = total_erds(mean2D['rac']['erds'],mask_tis_nopis)
    erds_tot['gis_RACMO2'] = total_erds(mean2D['rac']['erds'],mask_gis_nopis)
    erds_name = {'er':'Erosion','dep':'Deposition','net':'Net'}
    def print_erds_row(component,mask):
        print erds_name[component]+' (Gt yr$^{-1}$) &',erds_tot[mask+'(3700,5,9)'][component],'&',\
                erds_tot[mask+'(3700,6,8)'][component],'&',erds_tot[mask+'(4700,5,9)'][component],'&',\
                erds_tot[mask+'(2700,5,9)'][component],'&',erds_tot[mask+'_RACMO2'][component],'\\\\'
    print
    print '\\tophline'
    print 'Component & (3700,5,9) & (3700,6,8) & (4700,5,9) & (2700,5,9) & RACMO2 \\\\'
    print '\\middlehline'
    print 'TIS w/o Peninsula & & & & & \\\\'
    print_erds_row('er','tis')
    print_erds_row('dep','tis')
    print_erds_row('net','tis')
    print '\\middlehline'
    print 'GIS w/o Peninsula & & & & & \\\\'
    print_erds_row('er','gis')
    print_erds_row('dep','gis')
    print_erds_row('net','gis')
    print '\\bottomhline'


# general maps, sectors and stations #
# ================================== #

locations = sectors.get_locations()

def scatter_stations():
    # station_color=plt.get_cmap('viridis')(0.45)
    for lname in locations[locations['plot']==1].index:
        xx, yy = locations['x'][lname],locations['y'][lname]
        va, ha = locations['va'][lname],locations['ha'][lname]
        xpad, ypad = locations['xpad'][lname],locations['ypad'][lname]
        long_name = locations['long_name'][lname]
        long_name = long_name.replace('NN','\n')
        text(xx+xpad,yy+ypad,long_name,ha=ha,va=va,zorder=11,fontsize=13,rotation=locations['rot'][lname])
        if locations['ltype'][lname] == 'station':
            scatter(xx,yy,s=15,color='w',edgecolors='k',lw=1,zorder=10)

sector_idx = {'MBL-Ross-margin':0, 'MBL-Ross-inland':5, 'VL':3, 'DDU-DC':6, \
        'LD-WL':1, 'ZGN-DA':2, 'MAW-LG':4, 'SYW-DF':9, 'DML-margin':8, 'DML':7}
sector_col = {sector_name:cmap_qual(sector_idx[sector_name]/9.) for sector_name in sector_list}
def scatter_obs_by_sector():
    color={}
    for sector_name in sector_list:
        valIJ, obsXY, sector_bin, sector_grid, coord_grid, obs_sh, obs_smb = sector[sector_name]
        scatter(obsXY['x'],obsXY['y'],s=10,c=sector_col[sector_name],marker='o',zorder=6)
def map_locations():
    h=6
    dx, dy = x1D[-1]-x1D[0], y1D[-1]-y1D[0]
    w = h*dx/dy
    fig = figure(figsize=(w,h))
    clf()
    subplots_adjust(left=0.01,right=0.99,bottom=0.01,top=0.99)
    pcolormesh(x2D-dh,y2D-dh,sh2D,cmap=plt.get_cmap('Blues_r'),vmin=-10000,vmax=4000)
    layout(axis_minmax=False,scale=False)
    scatter(obs['x'],obs['y'],s=2,c=obs_color,marker='o',zorder=5)
    scatter_stations()
    scatter_obs_by_sector()
    savefig('./fig/article/fig2_sectors_map.png',dpi=600)

"""
read_samba.py
Created by Cecile Agosta on 2017-01-17.
2017 __ULiege__
"""
# = IMPORT MODULES ========================================================== #
from __future__ import division # true division
from StringIO import StringIO

import pandas as pd # Python Data Analysis Library
import numpy as np # array module
# from pylab import *
import matplotlib.pyplot as plt

from os.path import exists # find if file exists

# My modules
import extract_dem
import projection
# ========================================================================== #

def read_obs(database):
    obs_dir = './data/observation/'
    if database == "samba":
        filename = obs_dir+'02042014_samba_database.csv'
        print "reading:",filename
        obs_tmp = pd.read_csv(filename,na_values=-9999)
        obs=pd.DataFrame(obs_tmp.index,columns=["id"])
        obs["database"]=["samba" for idx in obs.index]
        obs["lon"]=obs_tmp["lon"]
        obs["lat"]=obs_tmp["lat"]
        obs["smb"]=obs_tmp["smb"]
        obs["smb_err"]=[np.nan for idx in obs_tmp.index]
        obs["sh"]=obs_tmp["sh"]
        obs["yearstart"]=obs_tmp["yearstart"]
        obs.loc[obs["yearstart"]==0,"yearstart"]=np.nan
        obs["yearend"]=obs_tmp["yearend"]
        obs.loc[obs["yearend"]==0,"yearend"]=np.nan
    elif database == "sumup":
        filename = obs_dir+'SUMup_dataset_july2017_accumulation_on_land_ice.csv'
        print "reading:",filename
        obs_tmp0 = pd.read_csv(filename,na_values=-9999)
        obs_tmp = obs_tmp0[(obs_tmp0["Lat_dd"]<-60)]
        obs=pd.DataFrame(obs_tmp.index,columns=["id"])
        obs["database"]=["sumup" for idx in obs.index]
        obs["lon"]=obs_tmp["Long_dd"].values
        obs["lat"]=obs_tmp["Lat_dd"].values
        obs["smb"]=obs_tmp["Accum"].values*1000. # kg m-2 yr-1
        obs["smb_err"]=obs_tmp["Error"].values*1000. # kg m-2 yr-1
        obs["sh"]=obs_tmp["Elevation_meters"].values
        obs["yearstart"]=obs_tmp["Start_year"].values
        obs["yearend"]=obs_tmp["End_year"].values
        obsyear=obs_tmp["Year"].values
        yearnan=np.isnan(obs["yearstart"])|np.isnan(obs["yearend"])
        obs.loc[yearnan,"yearstart"]=obsyear[yearnan]
        obs.loc[yearnan,"yearend"]=obsyear[yearnan]
    elif database == "wang":
        filename = obs_dir+'smb20c_SAMBA_Wang_update_2016.csv'
        print "reading:",filename
        obs_tmp = pd.read_csv(filename,na_values=-9999)
        obs=pd.DataFrame(obs_tmp.index,columns=["id"])
        obs["database"]=obs_tmp["dataset"]
        obs["ref"]=obs_tmp["RefComp"]
        obs["lon"]=obs_tmp["Longitude"]
        obs["lat"]=obs_tmp["Latitude"]
        obs["smb"]=obs_tmp["SMB"]
        obs["smb_err"]=[np.nan for idx in obs_tmp.index]
        obs["sh"]=obs_tmp["Elevation"]
        obs["yearstart"]=obs_tmp["YearStart"]
        obs.loc[obs["yearstart"]==0,"yearstart"]=np.nan
        obs["yearend"]=obs_tmp["YearEnd"]
        obs.loc[obs["yearend"]==0,"yearend"]=np.nan
    obs["name"]=[obs["database"][idx]+str(obs["id"][idx]) for idx in obs.index]
    obs=obs.set_index(['name'])
    sh_bedmap2, slope_bedmap2 = extract_dem.bedmap2(obs['lon'],obs['lat'],get_slope=True)
    obs['sh_bedmap2']=sh_bedmap2
    obs['slope'] = slope_bedmap2
    obs.loc[np.isnan(obs['sh']),'sh']=obs['sh_bedmap2'][np.isnan(obs['sh'])]
    obs['yearnb'] = obs['yearend']-obs['yearstart']+1
    return obs

def one_obs_by_location(obs_tmp,yearlim=1950):
    yrstart=(obs_tmp['yearstart']>=yearlim)
    yrend=(obs_tmp['yearend']>=yearlim)
    obs=obs_tmp[yrstart&yrend]
    # define each location -> ll for "lonlat label"
    lab_ll=['lon'+str(round(obs['lon'][name],4))+'lat'+str(round(obs['lat'][name],4)) for name in obs.index]
    obs=obs.assign(lab_ll=lab_ll)
    # group by lon lat
    obs_ll_count=obs['lon'].groupby(obs['lab_ll']).count()
    obs_ll=pd.DataFrame(obs_ll_count.values,columns=["count"],index=obs_ll_count.index)
    obs_ll=obs_ll.assign(ref=obs['ref'].groupby(obs['lab_ll']).first())
    obs_ll=obs_ll.assign(database=obs['database'].groupby(obs['lab_ll']).first())
    obs_ll=obs_ll.assign(id=obs['id'].groupby(obs['lab_ll']).first())
    names=[obs_ll["database"][idx]+str(obs_ll["id"][idx]) for idx in obs_ll.index]
    obs_ll=obs_ll.assign(name=names)
    # ids list, if one want to retrieve the original values from obs_tmp
    obs_grouped=obs_ll[obs_ll['count']>1]
    ids_ll={obs_ll['name'][ll_name]:obs['id'].groupby(obs['lab_ll']).get_group(ll_name) for ll_name in obs_grouped.index}
    # compute values for each location
    obs_ll=obs_ll.assign(lon=obs['lon'].groupby(obs['lab_ll']).mean().values)
    obs_ll=obs_ll.assign(lat=obs['lat'].groupby(obs['lab_ll']).mean().values)
    obs_ll=obs_ll.assign(sh=obs['sh'].groupby(obs['lab_ll']).mean().values)
    obs_ll=obs_ll.assign(sh_bedmap2=obs['sh_bedmap2'].groupby(obs['lab_ll']).mean().values)
    obs_ll=obs_ll.assign(yearstart=obs['yearstart'].groupby(obs['lab_ll']).min().values)
    obs_ll=obs_ll.assign(yearend=obs['yearend'].groupby(obs['lab_ll']).max().values)
    obs_ll=obs_ll.assign(yearnb=obs['yearnb'].groupby(obs['lab_ll']).sum().values)
    obs_ll['x'],obs_ll['y'] = projection.stereosouth_lonlat2xy(obs_ll['lon'],obs_ll['lat'])
    # smb weighted with number of years
    # yearstart and yearend are different from the real time span of the dataset, but not too much (I checked)
    # smb
    smbtot=obs['yearnb']*obs['smb']
    smb_ll=smbtot.groupby(obs['lab_ll']).sum().values/obs_ll['yearnb']
    obs_ll=obs_ll.assign(smb=smb_ll)
    # smb std (34)
    smbstd=obs['smb'].groupby(obs['lab_ll']).std().values
    smbstd[obs_ll['count']<10]=np.nan
    obs_ll=obs_ll.assign(smb_std=smbstd)
    # smb err (6)
    obs_ll=obs_ll.assign(smb_err=obs['smb_err'].groupby(obs['lab_ll']).mean().values)
    # rename groups
    obs_ll=obs_ll.set_index(['name'])
    return obs_ll,ids_ll
#!/usr/bin/env python
# encoding: utf-8
'''
shade.py
Created by Cécile Agosta: jan 2018
Last modified: 22 march 2018
__ULiege__ __UGA__ __LSCE__
'''
# = IMPORT MODULES ========================================================== #
import matplotlib as mpl
from pylab import *
from matplotlib.pyplot import *
from matplotlib import gridspec
from numpy import * # array module
import numpy.ma as ma # masked array
# ========================================================================== #
# define the axes extent in 2D
xmin=-2800
xmax=2800
ymin=-2800
ymax=2800

def def_cmaps():
    cmap_seq = plt.get_cmap('viridis')
    cmap_smb = plt.get_cmap('YlGnBu')
    cmap_qual = plt.get_cmap('tab10')
    cmap_div = plt.get_cmap('BrBG') # RdBu_r
    cmap_BuRd = plt.get_cmap('RdBu_r')
    obs_color = plt.get_cmap('plasma')(0.45)
    sh_color = plt.get_cmap('gray')(0.5)
    return cmap_seq, cmap_smb, cmap_div, cmap_qual, cmap_BuRd, obs_color, sh_color
sh_color = plt.get_cmap('gray')(0.5)

def set_fig():
    fw=4.72441 # 12 cm in inches 
    left, right, top, bottom = 0.1, 0.5, 0.1, 0.1
    wspace = 0.1
    wcbar = 0.15
    w = (fw-wspace-wcbar-left-right)
    fh = w+top+bottom
    fig = figure(figsize=(fw,fh))
    gs = gridspec.GridSpec(1, 2, width_ratios=[1.,wcbar/w])
    gs.update(left=left/fw,right=1.-right/fw,bottom=bottom/fh,top=1.-top/fh,wspace=wspace/(fw/2))
    clf()
    subplot(gs[0])
    return fig, gs

def norm_cmap(cmap,bounds):
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmap_discr = matplotlib.colors.LinearSegmentedColormap.from_list('cmap_discrete',cmaplist, cmap.N)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    return norm

def compute_dh(x2D):
    dh = (x2D[1,0] - x2D[0,0])/2.
    return dh

def axisminmax(axis_minmax):
    if axis_minmax:
        axis([xmin,xmax,ymin,ymax])

def scontour(x2D,y2D,var2D,levels,lw=1.,zorder=10,lc='k',ls='-',axis_minmax=True):
    contour(x2D,y2D,var2D,levels,linewidths=lw,zorder=zorder,colors=lc,linestyles=ls)
    axisminmax(axis_minmax)

def spcolor(x2D,y2D,var2D,cmap,bounds,axis_minmax=True):
    dh = compute_dh(x2D)
    pcolormesh(x2D-dh,y2D-dh,var2D,cmap=cmap,norm=norm_cmap(cmap,bounds))
    axisminmax(axis_minmax)

def sshade_under(x2D,y2D,var2D,level,cmap=plt.get_cmap('plasma_r'),alpha=0.1,vmin=0.,vmax=2.,lc='fuchsia',lw=0.8,axis_minmax=True):
    dh = compute_dh(x2D)
    pcolormesh(x2D-dh,y2D-dh,ma.array(var2D<level,mask=var2D>level),cmap=cmap,alpha=alpha,vmin=vmin,vmax=vmax)
    scontour(x2D,y2D,var2D,[level],lw=lw,zorder=100,lc=lc,axis_minmax=axis_minmax)

def plot_graticules(x2D,y2D,lon2D,lat2D,axis_minmax=True):
    lon2D_out = ma.array(lon2D,mask=where((lat2D>=-80.01)&(lon2D>-170),0,1))
    scontour(x2D,y2D,lat2D,linspace(-80,-50,4),lw=0.5,ls='--',lc='gray',axis_minmax=axis_minmax)
    scontour(x2D,y2D,lon2D_out,linspace(-135,180,8),lw=0.5,ls='--',lc='gray',axis_minmax=axis_minmax)

def layout(x2D,y2D,lon2D,lat2D,sh2D,lsm2D,ground2D,square=False,ground=False,scale=True,axis_minmax=True):
    scontour(x2D,y2D,lsm2D,[0.5],lw=0.5,lc='k',axis_minmax=axis_minmax)
    scontour(x2D,y2D,sh2D,range(1000,4200,1000),lw=0.5,lc=sh_color,axis_minmax=axis_minmax)
    if ground:
        scontour(x2D,y2D,ground2D,[0.5],lw=0.5,lc='indianred',axis_minmax=axis_minmax)
    plot_graticules(x2D,y2D,lon2D,lat2D,axis_minmax=axis_minmax)
    if square:
        plot([-2500,-1500],[-2500,-2500],lw=1,color='k')
        plot([-2500,-2500],[-2500,-1500],lw=1,color='k')
        plot([-2500,-1500],[-1500,-1500],lw=1,color='k')
        plot([-1500,-1500],[-2500,-1500],lw=1,color='k')
    if scale:
        plot([-1000,0],[-1600,-1600],lw=3,color='k')
        text(-500,-1700,'1000 km',va='top',ha='center')
    gca().set_xticklabels([])
    gca().set_yticklabels([])

def plot_cbar(gs,cmap,bounds,extend='both'):
    axb1 = subplot(gs[1])
    if extend=='both':
        ticks = bounds[1:-1]
    elif extend=='max':
        ticks = bounds[:-1]
    elif extend=='min':
        ticks = bounds[1:]
    else:
        print 'Error in plot_cbar, extend keyword'
        return
    mpl.colorbar.ColorbarBase(axb1,cmap=cmap,norm=norm_cmap(cmap,bounds),ticks=ticks,extend=extend)

def top_label(label):
    text(0,2765,label,ha='center',va='top',fontsize='large')

def subfig_label(label,dx=0,dy=0):
    text(-2765+dx,2765+dy,label,ha='left',va='top',fontsize='large')

def top_sublabel(label,dx=0,dy=0):
    text(0+dx,2450+dy,label,va='top',ha='center')

def bottom_label(x2D,y2D,label,lc=None,lw=None,xx=-1700,yy=-2500,area=False,cmap=plt.get_cmap('plasma_r'),\
        alpha=0.1,vmin=0,vmax=2):
    dh = compute_dh(x2D)
    if area:
        if lc is None:
            lc='fuchsia'
        if lw is None:
            lw=0.8
        rect=(x2D>xx-400)&(x2D<xx-100)&(y2D>yy+10)&(y2D<yy+186)
        pcolormesh(x2D-dh,y2D-dh,ma.array(rect,mask=invert(rect)),cmap=cmap,alpha=alpha,vmin=vmin,vmax=vmax)
        contour(x2D,y2D,rect,[0.5],linewidths=lw,zorder=100,colors=lc)
        text(xx,yy+10,label,va='bottom',ha='left')
    else:
        if lc is None:
            lc='k'
        if lw is None:
            lw=1.2
        plot([xx-400,xx-100.],[yy,yy],c=lc,lw=lw)
        text(xx,yy,label,va='center',ha='left')

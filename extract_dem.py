##/usr/bin/env python
# encoding: utf-8
"""
extract_dem.py
Created by Cécile Agosta on 2017-10-21
2017 __ULiege__.
+-------------------------------------------------------------------+
| Cecile Agosta, October 2017                                       |
| Input : in_lon : longitude (degree) of the NST grid               |
| ^^^^^^^ in_lat : latitude  (degree) of the NST grid               |
|                                                                   |
| Output: out_sh : surface elevation from Bamber                    |
| ^^^^^^^                                                           |
|                                                                   |
| Method:                                                           |
| ^^^^^^^                                                           |
|                                                                   |
| Data  :                                                           |
| ^^^^^^^                                                           |
| File name        : bedmap2.nc                                     |
| Dataset Title    : Bedmap2                                        |
|                                                                   |
| Dataset Creator  : British Antarctic Survey                       |
|                                                                   |
| Dataset Release Place: Cambridge, United Kingdom                  |
| Dataset Publisher: British Antarctic Survey                       |
| Online Resource  : http://www.antarctica.ac.uk//bas_research/     |
|                    our_research/az/bedmap2/                       |
| Reference: Fretwell P., Pritchard H.D., Vaughan D.G., Bamber J.L.,|
| ^^^^^^^^^       Barrand N.E., Bell R.E., Bianchi C., Bingham R.G.,|
|               Blankenship D.D.,Casassa G., Catania G., Callens D.,|
|            Conway H., Cook A.J., Corr H.F.J., Damaske D., Damm V.,|
|                    Ferraccioli F., Forsberg R., Fujita S., Gim Y.,|
|           Gogineni P., Griggs J.A., Hindmarsh R.C.A., Holmlund P.,|
|          Holt J.W., Jacobel R.W., Jenkins A., Jokat W., Jordan T.,|
|                   King E.C., Kohler J., Krabill W., Riger-Kusk M.,|
|          Langley K.A., Leitchenkov G., Leuschen C., Luyendyk B.P.,|
|        Matsuoka K., Mouginot J., Nitsche F.O., Nogi Y., Nost O.A.,|
|         Popov S.V., Rignot E., Rippin D.M., Rivera A., Roberts J.,|
|     Ross N., Siegert M.J., Smith A.M., Steinhage D., Studinger M.,|
|             Sun B., Tinto B.K., Welch B.C., Wilson D., Young D.A.,|
|                                Xiangbin C., & Zirizzotti A. (2013)|
|      Bedmap2: improved ice bed, surface and thickness datasets for|
|                            Antarctica. The Cryosphere, 7, 375–393.|
|        http://www.the-cryosphere.net/7/375/2013/tc-7-375-2013.pdf |
|                                                                   |
| Metadata :                                                        |
| ^^^^^^^^^^                                                        |
|    samples = 6667                                                 |
|    lines   = 6667                                                 |
|    map info = {Polar Stereographic South, 1, 1,                   |
|           -3333500., 3333500., 1000., 1000., WGS-84, units=Meters}|
|    projection info = {31, 6378137.0, 6356752.3, -71., 0.,         |
|         0.0, 0.0, WGS-84, Polar Stereographic South, units=Meters}|
|                                                                   |
+-------------------------------------------------------------------+
"""
# = IMPORT MODULES ========================================================== #
from __future__ import division # true division
from netCDF4 import Dataset
import operator
import pandas as pd # Python Data Analysis Library
import numpy as np # array module
from numpy import *
import struct # unpack binary
import projection
# ========================================================================== #


def bedmap2(lon_list,lat_list,verbose=False,get_slope=False):
    # dem file
    dem_dir = "./data/dem/"
    dem_file = dem_dir + "bedmap2.nc"
    slope_file = dem_dir + "bedmap2_slope.nc"
    # characteristics of the DEM grid
    dem_x0  = -3333.5
    dem_y0  = -3333.5
    dem_dh  =  1.
    mx = 6667
    my = 6667
    # open and read bedmap2 data file
    if verbose:
        print "Topography : Bedmap2 (2013) Antarctic 1km DEM"
        print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    nc_dataset=Dataset(dem_file)
    dem_sh_1km=nc_dataset.variables['sh']
    if get_slope:
        nc_dataset=Dataset(slope_file)
        dem_slope_1km=nc_dataset.variables['slope']
    # convert lon lat to stereographic south
    xx_list,yy_list = projection.stereosouth_lonlat2xy(lon_list,lat_list)
    xy_list = np.transpose(np.array([xx_list,yy_list]))
    # extract dem on dataset
    sh = []
    slope = []
    for xx,yy in xy_list:
        dem_ii = int(rint(xx - dem_x0 + 1)) - 1
        dem_jj = int(rint(yy - dem_y0 + 1)) - 1
        if dem_ii<mx and dem_jj<my:
            dem_value = dem_sh_1km[dem_jj,dem_ii]
            if get_slope:
                slope_value = dem_slope_1km[dem_jj,dem_ii]
            if dem_value > -999 and not isnan(dem_value):
                sh.append(dem_value)
                if get_slope:
                    slope.append(slope_value)
            else:
                sh.append(np.nan) 
                if get_slope:
                    slope.append(np.nan)
        else:
            sh.append(np.nan)
            if get_slope:
                slope.append(np.nan)
            if verbose:
                print xx,yy,dem_ii,dem_jj
    if get_slope:
        return np.array(sh), np.array(slope)
    else:
        return np.array(sh)

# lon_list,lat_list = [0.,90.], [-80.,-70.]
# extract_sh_bamber(lon_list,lat_list,verbose=True)

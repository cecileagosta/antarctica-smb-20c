#!/usr/bin/env python
# encoding: utf-8
"""
readwrite.py

Created by Cécile Agosta on 2013-01-25.
Copyright (c) 2013 __ULg__. All rights reserved.
"""
### IMPORT MODULES ##############################################################################
from __future__ import division     # true division

from netCDF4 import Dataset
import netCDF4 as nc

import datetime
import numpy as np # array module
import numpy.ma as ma # masked array
from StringIO import StringIO
from pylab import *
from time import * # module to measure time

import csv

from mpl_toolkits.axes_grid1 import host_subplot
# from mpl_toolkits.basemap import Basemap, cm

from matplotlib.transforms import Bbox
from matplotlib.path import Path
from matplotlib.patches import Rectangle
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt

from scipy import spatial

import operator

import array

from scipy import stats
#################################################################################################

def read_nc(filename,var_name_list,conversion=None,verbose=False,lev=False,dtime=False):
    if verbose:
        print "Open ",filename
    if conversion is None:
        conversion=[None]*len(var_name_list)
    nc_dataset = Dataset(filename)
    var_list=[]
    k=0
    for var_name in var_name_list:
        if verbose:
            print "Read : ",var_name
        var_ini=nc_dataset.variables[var_name]
        ndim=var_ini.ndim
        if verbose:
            print " > ndim :",ndim
        if ndim==1:
            var=var_ini[:]
        elif ndim==2:
            var=var_ini[:,:]
        elif ndim==3:
            if var_ini.shape[0]==1:
                var=var_ini[0,:,:]
            else:
                var=var_ini[:,:,:]
        elif ndim==4:
            # nk=var_ini.shape[1]-1
            nk=0
            if var_ini.shape[0]==1:
                if lev:
                    var=var_ini[0,:,:,:]
                else:
                    var=var_ini[0,nk,:,:]
            else:
                if lev:
                    var=var_ini[:,:,:,:]
                else:
                    var=var_ini[:,nk,:,:]
        if conversion[k] is not None:
            if conversion[k] in ['date','greg','date_from_360dayyears']:
                conv=conversion[k]
                ncnum=var
                if verbose:
                    print " > conversion :",conv
                if conv == 'date':
                    ncunit,nccalendar = read_date_features(filename,var_name)
                if conv == 'greg':
                    ncunit,nccalendar = read_date_features(filename,var_name,iscal=False)
                if conv == 'date_from_360dayyears':
                    ncnum = ncnum*30*12
                    ncunit,nccalendar = read_date_features(filename,var_name)
                    nccalendar = "360_day"
                    ncunit="days"+ncunit[5:]
                var = convert_ncnum_to_pynum(ncnum,ncunit,nccalendar)
                if dtime:
                    tunit_py='days since 0001-01-01 00:00:00'
                    var = nc.num2date(var+1,tunit_py)
            else:
                var = var*conversion[k]
        var=np.array(var)
        var_list.append(var)
        k+=1
    return var_list

def read_axis_nc(filename,var_name,verbose=False,iscal=True):
    if verbose:
        print "Open ",filename
    nc_dataset = Dataset(filename)
    var=nc_dataset.variables[var_name]
    axis_list=[]
    for axis_name in var.dimensions:
        axis = nc_dataset.variables[axis_name][:]
        if axis_name == 'TIME':
            ncnum=axis
            ncunit,nccalendar = read_date_features(filename,axis_name,iscal=iscal)
            axis = convert_ncnum_to_pynum(ncnum,ncunit,nccalendar)
        axis_list.append(axis)
    return axis_list


def convert_ncnum_to_pynum(ncnum,ncunit,nccalendar):
    if ncunit == 'month(360_days) since 1901-01-15 00:00:00':
        ncunit = 'days since 1901-01-15 00:00:00'
        ncnum  = ncnum*30
    date_nc = nc.num2date(ncnum,ncunit,calendar=nccalendar)
    unit_py = 'days since 0001-01-01 00:00:00'
    num_py  = nc.date2num(date_nc,unit_py)-1
    return num_py

def read_date_features(filename,time_name,iscal=True):
    nc_dataset = Dataset(filename)
    unit = nc_dataset.variables[time_name].units
    if iscal:
        cal  = nc_dataset.variables[time_name].calendar
    else:
        cal  = 'standard'
    if cal == '360_DAYS' or cal == '360_DAY':
        cal = '360_day'
    return unit,cal

def read_data(DS):
    if isinstance(DS[0], float) or isinstance(DS[0], int) or isinstance(DS[0], str) :
        ndim = 1
    else:
        ndim = len(DS[:][0])
    if ndim is 1:
        Data_out = np.array(DS)
    else:
        Data_nb  = len(DS)
        Data_out = []
        for k in range(ndim) :
            Data_out.append(np.array([DS[i][k] for i in range(Data_nb)]))
    return Data_out

def read_text(filename, skip_header=0, delimiter=";",names=None,formats=None):
    if names is None or formats is None:
        DS = np.genfromtxt(filename, skip_header=1, delimiter=delimiter)
    else:
        dt = np.dtype({'names': names,'formats': formats})
        DS = np.genfromtxt(filename, dtype=dt, delimiter=delimiter,skip_header=skip_header)
    return read_data(DS)

def var_append(var_list,var_local):
    if len(var_list)!=len(var_local):
        print "ERROR : the two lists must have the same lenght"
    else:
        for v in range(len(var_list)):
            var_list[v].append(var_local[v])
    return var_list

def write_csv(filename,DS,names=None) :
    f = open(filename, 'w+') 
    c = csv.writer(f,delimiter=';')
    nbVar=len(DS)
    nbData=len(DS[0])
    if names is not None :
        c.writerow(names)
    for data in range(nbData) :
        c.writerow([DS[var][data] for var in range(nbVar)])
        # f.write([DS[var][data] for var in range(nbVar)])
    f.close()
    return

def read_csv(filename,skip_header=0,delimiter=';',col_types=None) :
    f = open(filename, 'r+') 
    DS = csv.reader(f,delimiter=delimiter)
    if skip_header:
        DS.next()
    first_row = next(DS)
    num_cols = len(first_row)
    Data_in = DS
    Data_out = []
    if col_types is not None:
        Data_in = []
        for row in DS:
            row = [convert(value) for convert, value in zip(col_types, row)]
            Data_in.append(row)
    for column in range(num_cols):
        Data_out.append(np.array([row[column] for row in Data_in]))
    f.close()
    return Data_out


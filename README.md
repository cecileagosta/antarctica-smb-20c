Contact: [Cécile Agosta](mailto:cecile.agosta@gmail.com)

**My python code for creating figures and tables of the article:**  
C. Agosta, C. Amory, C. Kittel, A. Orsi, V. Favier, H. Gallée, M.R. van den Broeke, J.T.M. Lenaerts , J.M. van Wessem, and X. Fettweis: Estimation of the Antarctic surface mass balance using MAR (1979–2015) and identification of dominant processes, The Cryosphere Discuss., [doi:10.5194/tc-2018-76](https://doi.org/10.5194/tc-2018-76), in review, 2018. 

**IMPORTANT**: the data/ directory should be downloaded here: [https://drive.google.com/open?id=1ll6QACbu5_cHzasD-wXzlAB2JHIhtLc1](https://drive.google.com/open?id=1ll6QACbu5_cHzasD-wXzlAB2JHIhtLc1)

**scripts:**  
1. antarctica-20c-smb.py  
2. antarctica-20c-density.py  

**modules:**  
- curvature  
- extract\_data  
- extract\_dem  
- projection  
- read\_model  
- read\_obs  
- readwrite  
- sectors  
- shade  

**how to run it:**  
`ipython`  
```
%run antarctica-20c-smb.py
%pylab  
```

and then launch the function you want, e.g.:  
```
fig1()
fig2()
fig3()
fig4()
fig5()
table1()
table2()
tableS2()
figS3()
figS4()
figS5()
figS6()
figS7()
figS8()
figS9()
```

and:  
`ipython`  
```
%run antarctica-20c-density.py
%pylab
figS1()
tableS1()
```

If you use those scripts, I would be happy to receive news of what you did with it: cecile.agosta@gmail.com. And if you find bugs (which is likely), write me too.  



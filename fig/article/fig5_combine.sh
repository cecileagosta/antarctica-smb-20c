figa="fig5_dsmb_MAR-RACMO2.png"
figb="fig5_dsnf_crests.png"
figc="fig5_dsnf_valleys.png"
figd="fig5_dsnf_rel_Grazioli.png"

convert $figa $figb +append fig5_tmp1.png
convert $figc $figd +append fig5_tmp2.png
convert fig5_tmp1.png fig5_tmp2.png -append fig5.png
rm -f  fig5_tmp1.png fig5_tmp2.png

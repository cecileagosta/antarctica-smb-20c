nb="4"
figa="fig${nb}_scatter_dsmb_vs_curv_all.png"
figb="fig${nb}_legend.png"
figc="fig${nb}_erds_curvature_vmin5_vmax9.png"
figd="fig${nb}_erds_RACMO2.png"

[[ ! -f $figb ]] && convert -density 700 -quality 100 fig4_legend.pdf fig4_legend.png

convert $figa $figb +append fig${nb}_tmp1.png
convert $figc $figd +append fig${nb}_tmp2.png
convert fig${nb}_tmp1.png fig${nb}_tmp2.png -append fig${nb}.png
rm -f  fig${nb}_tmp1.png fig${nb}_tmp2.png


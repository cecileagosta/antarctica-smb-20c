nb="1"
figa="fig${nb}_MAR_annual_SMB_mean.png"
figb="fig${nb}_MAR_annual_SMB_std.png"
figc="fig${nb}_MAR_annual_SMB_std-mean.png"
figd="fig${nb}_MAR-vs-obs_SMB_absolute.png"

convert $figa $figb +append fig${nb}_tmp1.png
convert $figc $figd +append fig${nb}_tmp2.png
convert fig${nb}_tmp1.png fig${nb}_tmp2.png -append fig${nb}.png
rm -f  fig${nb}_tmp1.png fig${nb}_tmp2.png


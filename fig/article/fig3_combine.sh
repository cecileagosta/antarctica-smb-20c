nb="3"
figa="fig${nb}_dsmb_LD-WL.png"
figb="fig${nb}_dsmb_ZGN-DA.png"
figc="fig${nb}_dsmb_MAW-LG.png"
figd="fig${nb}_dsmb_SYW-DF.png"

convert $figa $figb +append fig${nb}_tmp1.png
convert $figc $figd +append fig${nb}_tmp2.png
convert fig${nb}_tmp1.png fig${nb}_tmp2.png -append fig${nb}.png
rm -f  fig${nb}_tmp1.png fig${nb}_tmp2.png


SX="S1"
figa="fig${SX}_map02.png"
figb="fig${SX}_map05.png"
figc="fig${SX}_map1.png"
figd="fig${SX}_scatter.png"

convert $figa $figb +append fig${SX}_tmp1.png
convert $figc $figd +append fig${SX}_tmp2.png
convert fig${SX}_tmp1.png fig${SX}_tmp2.png -append fig${SX}.png
rm -f  fig${SX}_tmp1.png fig${SX}_tmp2.png

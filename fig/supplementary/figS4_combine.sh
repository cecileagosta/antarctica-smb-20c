figa="figS4_dsmb_MAR-JRA-55.png"
figb="figS4_dsmb_MAR-MERRA2.png"
figc="figS4_dsmb_rel_MAR-JRA-55.png"
figd="figS4_dsmb_rel_MAR-MERRA2.png"

convert $figa $figb +append figS4_tmp1.png
convert $figc $figd +append figS4_tmp2.png
convert figS4_tmp1.png figS4_tmp2.png -append figS4.png
rm -f  figS4_tmp1.png figS4_tmp2.png

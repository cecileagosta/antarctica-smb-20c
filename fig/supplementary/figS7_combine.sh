Snb="S7"
figa="fig${Snb}_annual_smb.png"
figb="fig${Snb}_annual_snf.png"
figc="fig${Snb}_annual_sbl.png"
figd="fig${Snb}_annual_mlt.png"

convert $figa $figb +append fig${Snb}_tmp1.png
convert $figc $figd +append fig${Snb}_tmp2.png
convert fig${Snb}_tmp1.png fig${Snb}_tmp2.png -append fig${Snb}.png
rm -f  fig${Snb}_tmp1.png fig${Snb}_tmp2.png

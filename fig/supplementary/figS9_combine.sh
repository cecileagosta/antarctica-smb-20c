Snb="S9"
figa="fig${Snb}_dsmb_MAR-RACMO2.png"
figb="fig${Snb}_dsnf_MAR-RACMO2.png"
figc="fig${Snb}_dsmb_rel_MAR-RACMO2.png"
figd="fig${Snb}_dsnf_rel_MAR-RACMO2.png"

convert $figa $figb +append fig${Snb}_tmp1.png
convert $figc $figd +append fig${Snb}_tmp2.png
convert fig${Snb}_tmp1.png fig${Snb}_tmp2.png -append fig${Snb}.png
rm -f  fig${Snb}_tmp1.png fig${Snb}_tmp2.png

Snb="S8"

pre="fig${Snb}_R_dsmb_vs_curv"
convert ${pre}_LD-WL.png ${pre}_ZGN-DA.png ${pre}_MAW-LG.png ${pre}_SYW-DF.png +append fig${Snb}_tmp1.png
pre="fig${Snb}_scatter_dsmb_vs_curv"
convert ${pre}_LD-WL.png ${pre}_ZGN-DA.png ${pre}_MAW-LG.png ${pre}_SYW-DF.png +append fig${Snb}_tmp2.png
convert fig${Snb}_tmp1.png fig${Snb}_tmp2.png -append fig${Snb}.png
rm -f  fig${Snb}_tmp1.png fig${Snb}_tmp2.png



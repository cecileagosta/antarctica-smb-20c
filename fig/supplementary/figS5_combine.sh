Snb="S5"
figa="fig${Snb}_sbl_mar_ERA-Interim.png"
figb="fig${Snb}_sbl_rac.png"
figc="fig${Snb}_susn_rac.png"
figd="fig${Snb}_suds_rac.png"

convert $figa $figb +append fig${Snb}_tmp1.png
convert $figc $figd +append fig${Snb}_tmp2.png
convert fig${Snb}_tmp1.png fig${Snb}_tmp2.png -append fig${Snb}.png
rm -f  fig${Snb}_tmp1.png fig${Snb}_tmp2.png

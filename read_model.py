#!/usr/bin/env python
# encoding: utf-8
"""
read_model_data.py
Created by Cécile Agosta on 2014-05-15.
2013 __ULiege__.
"""
# = IMPORT MODULES ========================================================== #
# ipython -pylab for interactive plot
from __future__ import division # true division
from netCDF4 import Dataset
import netCDF4 as nc
from calendar import monthrange
import datetime
import numpy as np # array module
import numpy.ma as ma # masked array
from StringIO import StringIO
from pylab import *
from time import * # module to measure time
import csv
from mpl_toolkits.axes_grid1 import host_subplot
from matplotlib.transforms import Bbox
from matplotlib.path import Path
from matplotlib.patches import Rectangle
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
import operator
import array
import scipy
from scipy import stats
from scipy import spatial
from os.path import exists
# My modules
import readwrite
import projection
# ========================================================================== #

# user directory
mod_dir = './data/model/'
# date definition
unit_py = 'days since 0001-01-01 00:00:00'

def read_mar_grid():
    var_names = 'x','y','LON','LAT','GROUND','ICE','SH','AREA','basin_r'
    filename = mod_dir+"MARcst-AN35km-201x171.cdf"
    x, y, lon, lat, ground, ice, sh, area, basin = readwrite.read_nc(filename,var_names)
    ground = ground/100.
    area = area*35.*35.
    lsm = where(ice>=30.,1,0)
    ice = ice/100.
    xx, yy = scipy.meshgrid(x,y)
    mar2D={'x':x,'y':y,'x2D':xx,'y2D':yy,'lon':lon,'lat':lat,'area':area,'ground':ground,\
            'ice':ice,'lsm':lsm,'sh':sh,'basin':basin}
    return mar2D

def read_mar_monthly(rean,var_name='smb',read_date=True):
    filename = mod_dir+"MAR-"+rean+"_"+var_name+"_20c_monthly.nc4"
    print "reading:",filename
    var = readwrite.read_nc(filename,[var_name])[0]
    var[var>=9999.]=np.nan
    var = var*12 # kg m-2 a-2
    if read_date:
        date = readwrite.read_nc(filename,['time'],conversion=['greg'])[0]
        dtime = nc.num2date(date+1,unit_py)
        return var, dtime
    else:
        return var

def read_mar_annual(var_name,lsm2D,rean='ERA-Interim',year_min=1979,year_max=2015):
    filename = mod_dir+'MAR-'+rean+'_'+var_name+'_20c_annual.nc4'
    print "reading:",filename
    mean3D = readwrite.read_nc(filename,[var_name])[0]
    year_range = range(mean3D.shape[0])
    lsm3D = np.array([lsm2D for year in year_range])
    if rean == 'MERRA2':
        lsm3D[0,:,:] = lsm2D*0+1
    mean3D = ma.array(mean3D,mask=lsm3D)
    mean2D, std2D = mean3D.mean(axis=0), mean3D.std(axis=0)
    return mean2D, std2D, mean3D

def read_rac_grid():
    filename = mod_dir+"Height_latlon_ANT27.nc4"
    lon2D_rac0,lat2D_rac0,lsm2D_rac0, sh2D_rac0 = readwrite.read_nc(filename,['lon2D','lat2D','lsm2d','Height'])
    x2D_rac0, y2D_rac0 = projection.stereosouth_lonlat2xy(lon2D_rac0,lat2D_rac0)
    return x2D_rac0, y2D_rac0, lsm2D_rac0, sh2D_rac0

def regrid_rac_to_mar(var2D_rac,x2D_rac,y2D_rac,x2D_mar,y2D_mar,mask_in,mask_out):
    x1D_rac, y1D_rac = x2D_rac.flatten(), y2D_rac.flatten()
    var1D_rac = var2D_rac.flatten()
    if mask_in is None:
        mask_in = ones_like(var1D_rac,dtype=bool)
    else:
        mask_in = (mask_in.flatten()==1)
    var2D_out = scipy.interpolate.griddata((x1D_rac[mask_in], y1D_rac[mask_in]),var1D_rac[mask_in],\
            (x2D_mar,y2D_mar),method='linear')
    if mask_out is None:
        return var2D_out
    else:
        return ma.array(var2D_out,mask=mask_out)

def read_rac_annual(var_name,lsm2D,regrid,year_min=1979,year_max=2015):
    rac_smb_file = './data/out/RACMO2_'+var_name+'_annual_MARgrid.npy'
    year_range = range(year_min,year_max+1)
    lsm3D = np.array([lsm2D for year in year_range])
    if exists(rac_smb_file):
        print "loading:",rac_smb_file
        mean2D, std2D, mean3D = np.load(rac_smb_file)
        mean2D, std2D, mean3D = ma.array(mean2D,mask=lsm2D), ma.array(std2D,mask=lsm2D), ma.array(mean3D,mask=lsm3D)
    else:
        month_range = range(1,13)
        years = np.array([year for year in year_range for month in month_range])
        if var_name == 'susn':
            subl_monthly = read_rac_monthly0('sbl')
            suds_monthly = read_rac_monthly0('suds')
            var2D_rac0_monthly = subl_monthly - suds_monthly
        elif var_name == 'rnf':
            prec_monthly = read_rac_monthly0('prec')
            snf_monthly = read_rac_monthly0('snf')
            var2D_rac0_monthly = maximum(prec_monthly-snf_monthly,0)
        else:
            var2D_rac0_monthly = read_rac_monthly0(var_name)
        # annual values
        mean3D0 = {year:var2D_rac0_monthly[years==year,:,:].sum(axis=0) for year in year_range}
        if var_name in ['snf','rnf']:
            mean3D = np.array([regrid(mean3D0[year],mask_in=None,mask_out=None) for year in year_range])
        else:
            mean3D = np.array([regrid(mean3D0[year],mask_out=None) for year in year_range])
        if var_name == 'mlt':
            mean3D = maximum(mean3D,0.)
        mean3D = ma.array(mean3D,mask=lsm3D)
        # temporal mean and std
        mean2D, std2D = mean3D.mean(axis=0), mean3D.std(axis=0)
        # save data
        np.save(rac_smb_file,[mean2D.data,std2D.data,mean3D.data])
    return mean2D, std2D, mean3D

def read_rac_monthly(var_name,dates_index,lsm2D,regrid):
    mdtime = dates_index
    lsm3D = np.array([lsm2D for tt in range(dates_index.shape[0])])
    rac_smb_file = './data/out/RACMO2_'+var_name+'_monthly_MARgrid.npy'
    if exists(rac_smb_file):
        print "loading:",rac_smb_file
        smb2D = ma.array(np.load(rac_smb_file),mask=lsm3D)
    else:
        smb2D_month_rac0 = read_rac_monthly0('smb')*12.
        smb2D = ma.array([regrid(smb2D_month_rac0[tt,:,:]) for tt in range(dates_index.shape[0])],mask=lsm3D)
        np.save(rac_smb_file,smb2D.data)
    return smb2D, mdtime

def read_rac_monthly0(var_name,year_min=1979,year_max=2015):
    year_range = range(year_min,year_max+1)
    month_range = range(1,13)
    mtype = 'S'
    if var_name == 'suds':
        mtype = 'A'
    rac_name = {'sbl':'subl','erds':'erds','suds':'suds','rof':'runoff','mlt':'snowmelt',\
            'snf':'snowfall','prec':'precip','smb':'smb'}
    rname = rac_name[var_name]
    filename = mod_dir+rname+"_monthly"+mtype+"_ANT27_ERAINx_RACMO2.4_197901_201512.nc"
    print "reading:",filename
    var2D_rac0 = readwrite.read_nc(filename,[rname])[0]
    if mtype=='A':
        nb_day_by_month = np.array([monthrange(year,month)[1] for year in year_range for month in month_range])
        var2D_rac0 = var2D_rac0*nb_day_by_month[:,np.newaxis,np.newaxis]*3600*24 # kg m-2 s-1 -> kg m-2
    if var_name == 'sbl':
        var2D_rac0 = -var2D_rac0
    return var2D_rac0



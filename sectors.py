#!/usr/bin/env python
# encoding: utf-8
"""
sectors.py
Created by Cécile Agosta: nov 2017
Last modified: 21 nov 2017
__ULiege__
"""
# = IMPORT MODULES ========================================================== #
from __future__ import division # true division
import operator
import pandas as pd # Python Data Analysis Library
import numpy as np # array module
from numpy import *
import netCDF4 as nc # netcdf module
from netCDF4 import Dataset
from os.path import exists
import projection
# ========================================================================== #

def transect_to_sector2D(transect,xx,yy):
    y_top = (yy >= (xx-transect['x0'])*(transect['y1']-transect['y0']) / \
            (transect['x1']-transect['x0'])+transect['y0'])
    y_low = (yy <= (xx-transect['x0'])*(transect['y1']-transect['y0']) / \
            (transect['x1']-transect['x0'])+transect['y0']+transect['dy'])
    return (y_top)&(y_low)&(yy<transect['ymax'])&(yy>transect['ymin'])&(xx<transect['xmax'])&(xx>transect['xmin'])

def get_locations():
    locations = pd.read_csv('./data/map/sectors_location.csv',index_col='name')
    locations['x'], locations['y'] = projection.stereosouth_lonlat2xy(locations['lon'],locations['lat'])
    return locations

def get_sector2D(x2D,y2D,sh2D):
    transect={}
    transect['DDU-DC']={'x0':1610.,'y0':-1960.,'x1':1300.,'y1':-850.,'ymin':-1948,'ymax':-800.,'dy':400.,'xmin':-2800,'xmax':2800}
    transect['ZGN-DA']={'x0':920.,'y0':150.,'x1':2230.,'y1':430.,'ymin':200.,'ymax':550.,'dy':130.,'xmin':1031,'xmax':2800}
    transect['MAW-LG-x']={'x0':1300.,'y0':840.,'x1':2150.,'y1':1070.,'ymin':850.,'ymax':1150.,'dy':130.,'xmin':1382,'xmax':2047}
    transect['SYW-DF-x']={'x0':960.,'y0':1050.,'x1':1530.,'y1':1500.,'ymin':1030.,'ymax':1520.,'dy':130.,'xmin':893,'xmax':1453}
    transect['VL']={'x0':710.,'y0':-1960.,'x1':400.,'y1':-850.,'ymin':-2000.,'ymax':-960.,'dy':3200.,'xmin':-2800,'xmax':2800}
    transect['MBL-Ross-inland']={'x0':-400.,'y0':-680.,'x1':-1055.,'y1':-50.,'ymin':-925.,'ymax':-130.,'dy':300.,'xmin':-2800,'xmax':20}
    transect['MBL-Ross-margin']={'x0':-440.,'y0':-1200.,'x1':-1140,'y1':-720.,'ymin':-1210.,'ymax':-200.,\
            'dy':350.,'xmin':-1140,'xmax':-250}
    sector2D = {}
    for sector_name in list(transect):
        sector2D[sector_name] = transect_to_sector2D(transect[sector_name],x2D,y2D)
    sector2D['MAW-LG-y'] = (x2D>1290)&(x2D<1382)&(y2D>645)&(y2D<930)
    sector2D['SYW-DF-y'] = (x2D>1453)&(x2D<1520)&(y2D>1480)&(y2D<1780)
    sector2D['DML-margin'] = (x2D>780)&(x2D<860)&(y2D>1830)&(y2D<2020)
    sector2D['DML'] = (x2D>-400)&(x2D<790)&(y2D>-80)&(y2D<2200)&(sh2D>=1700.)
    sector2D['LD-y'] = (x2D>1710)&(x2D<2080)&(y2D>-1488)&(y2D<-998)
    sector2D['LD-x'] = (x2D>2083)&(x2D<2325)&(y2D>-998)&(y2D<-880)
    return sector2D

def get_longname(sector_name):
    sector_longname_dict = {}
    sector_longname_dict['DDU-DC'] = 'Dumont-d\'Urville$-$Dome C'
    sector_longname_dict['LD-y'] = 'Wilkes Land$-$Law Dome,'
    sector_longname_dict['LD-x'] = 'margin'
    sector_longname_dict['LD-WL'] = 'Law Dome$-$Wilkes Land'
    sector_longname_dict['ZGN-DA'] = 'Zhongshan$-$Dome A'
    sector_longname_dict['MAW-LG-x'] = 'Mawson$-$Lambert Glacier'
    sector_longname_dict['MAW-LG-y'] = 'inland'
    sector_longname_dict['MAW-LG'] = 'Mawson$-$Lambert Glacier'
    sector_longname_dict['SYW-DF-x'] = 'Syowa$-$Dome F, '
    sector_longname_dict['SYW-DF-y'] = 'margin'
    sector_longname_dict['SYW-DF'] = 'Syowa$-$Dome F'
    sector_longname_dict['DML-margin'] = 'Princ. Elisabeth'
    sector_longname_dict['VL'] = 'Victoria Land'
    sector_longname_dict['DML'] = 'Dronning Maud Land'
    sector_longname_dict['MBL-Ross-margin'] = 'Ross$-$Mary Byrd Land (margin)'
    sector_longname_dict['MBL-Ross-inland'] = 'Ross$-$Mary Byrd Land (inland)'
    if sector_name in list(sector_longname_dict):
        sector_longname = sector_longname_dict[sector_name]
    else:
        sector_longname = sector_name
    return sector_longname


def get_type(sector_name):
    if sector_name in ('MBL-Ross-margin','MBL-Ross-inland','VL','DML'):
        sector_type = "Scattered"
    elif sector_name in ('DDU-DC','LD-WL','ZGN-DA','MAW-LG','SYW-DF','DML-margin'):
        sector_type = "Transect"
    else:
        sector_type = "Unknown"
    return sector_type

def get_direction(sector_name):
    sector_direction_dict = {}
    if sector_name in ('DDU-DC','LD-y','MAW-LG-y','SYW-DF-y','DML-margin','MBL-Ross-inland'):
        return 'y'
    elif sector_name in ('LD-x','ZGN-DA','MAW-LG-x','SYW-DF-x','MBL-Ross-margin'):
        return 'x'
    elif sector_name in ('VL','DML'):
        return 'sh'
    elif sector_name in ('LD-WL','MAW-LG','SYW-DF'):
        return 'xy'
    else:
        print "Error in sectors.get_direction, this sector name is not defined"
        return None

def get_reverse(sector_name):
    if sector_name in ('MBL-Ross-margin','LD-x','LD-y','ZGN-DA','MAW-LG-x','MAW-LG-y','SYW-DF-x','SYW-DF-y','DML-margin'):
        return True
    else:
        return False

def sector_on_grid(sector2D,val_grid,selected=None):
    grid = val_grid
    if selected is not None:
        grid = val_grid[selected]
    sectorGrid = np.array([sector2D[mar_j-1,mar_i-1] \
                for (mar_j,mar_i) in zip(grid['mar_j'],grid['mar_i'])])
    return sectorGrid

def extract_sort(val_grid,sector2D,direction,selected=None):
    sector = sector_on_grid(sector2D,val_grid,selected=selected)
    var = val_grid
    if selected is not None:
        var = val_grid[selected]
    idx = index(direction)
    if direction is None:
        return var[sector]
    elif direction in ["x","y"]:
        return var[sector].sort_values(['mar_'+idx,direction])
    elif direction in ["sh"]:
        return var[sector].sort_values(['mar_sh','obs_sh'])
    else:
        print "Error in sectors.extract_sort, direction must be x, y or sh"
        return None

def sector_digitize(valIJ,obsXY,direction,nbbybin=10,dsh=200.):
    if direction in ["x","y"]:
        return valIJ['mar_'+index(direction)], obsXY['mar_'+index(direction)]
    elif direction in ["sh"]:
        # valnb = valIJ['mar_sh'].size
        # bin_range = range(0,valnb,nbbybin)
        # shbins = [valIJ['mar_sh'][i] for i in bin_range]
        # if valnb-bin_range[-1]-1>=nbbybin/2:
        #     shbins = shbins + [valIJ['mar_sh'][-1]+1.]
        # else:
        #     shbins[-1] = valIJ['mar_sh'][-1]+1.
        shmin = min(obsXY['mar_sh'].min(),valIJ['mar_sh'].min())-10.
        shmax = max(obsXY['mar_sh'].max(),valIJ['mar_sh'].max())+10.
        nbins = (shmax-shmin)/dsh
        shbins = linspace(shmin,shmax,nbins)
        return np.digitize(valIJ['mar_sh'], shbins), np.digitize(obsXY['mar_sh'], shbins)
    else:
        print "Error in sectors.digit, direction must be x, y or sh"
        return None, None

def sector_bin(valIJ,digit,rean_list,selected=None,test=False):
    # smb
    weights = np.minimum(valIJ['yearnb'],valIJ['yearend']-valIJ['yearstart']+1)
    sector_bin = {}
    for rean in list(rean_list)+['obs']:
        for var_name in '_smb','_smb_std':
            sector_bin[rean+var_name] = digit_average(valIJ[rean+var_name],weights,digit)
    # MAR smb min max
    rean_list_minmax = 'ERA-Interim','MERRA2','JRA-55'
    sector_bin['smb_MAR_min'], sector_bin['smb_MAR_max'] = minmax(rean_list_minmax,'_smb',sector_bin)
    # statistics on observations
    sector_bin['yearstart'] = valIJ['yearstart'].groupby(digit).min()
    sector_bin['yearend'] = valIJ['yearend'].groupby(digit).max()
    sector_bin['obs_nb'] = valIJ['obs_nb'].groupby(digit).sum()
    def smb_uncertainty(smb_list):
        '''
        uncertainty (spatial std) of observations
        see define_obs_uncertainty() for definition
        '''
        xlim = np.array([15.,25.,50.,250.,800.])
        ylim = np.array([1.,5.,30.,60.,302.])
        def slope_intercept(xx,yy):
            slope = (yy[1]-yy[0])/(xx[1]-xx[0])
            intercept = yy[1]-slope*xx[1]
            return slope, intercept
        ssp,iip = {},{}
        ssp['low'],iip['low'] = slope_intercept(xlim[1:3],ylim[1:3])
        ssp['int'],iip['int'] = slope_intercept(xlim[2:4],ylim[2:4])
        ssp['high'],iip['high'] = slope_intercept(xlim[3:],ylim[3:])
        std_list = []
        for smb_mean in smb_list:
            sect = 'very_low'
            if xlim[1] <= smb_mean <= xlim[2]:
                sect = 'low'
            elif xlim[2] <= smb_mean <= xlim[3]:
                sect = 'int'
            elif smb_mean >= xlim[3]:
                sect = 'high'
            if sect == 'very_low':
                std_list.append(5.)
            else:
                std_list.append(ssp[sect]*smb_mean+iip[sect])
        return np.array(std_list)
    sector_bin['obs_smb_inf'] = sector_bin['obs_smb']-\
            smb_uncertainty(sector_bin['obs_smb'])#*1.96/sqrt(sector_bin['obs_nb'])
    sector_bin['obs_smb_sup'] = sector_bin['obs_smb']+\
            smb_uncertainty(sector_bin['obs_smb'])#*1.96/sqrt(sector_bin['obs_nb'])
    # surface height
    sector_bin['mar_sh'] = digit_average(valIJ['mar_sh'],weights,digit)
    sector_bin['curv'] = digit_average(valIJ['curv'],weights,digit)
    sector_bin['rac_curv'] = digit_average(valIJ['rac_curv'],weights,digit)
    sector_bin['subl'] = digit_average(valIJ['subl'],weights,digit)
    sector_bin['ws'] = digit_average(valIJ['ws'],weights,digit)
    sector_bin['div'] = digit_average(valIJ['div'],weights,digit)
    # test CA
    if test:
        sector_bin['curvDyp'] = digit_average(valIJ['curvDyp'],weights,digit)
        sector_bin['curvDym'] = digit_average(valIJ['curvDym'],weights,digit)
        sector_bin['dw'] = digit_average(valIJ['dw'],weights,digit)
    return sector_bin

def bin_to_grid(sector_bin,digit):
    # conversion of grided values (IJ) to the real location of observation (XY) (stairs)
    sectorXY = {}
    for var_name in list(sector_bin):
        sectorXY[var_name] = np.array([sector_bin[var_name][dd] for dd in digit])
    return sectorXY

def index(direction):
    idx = 'i' if direction=='x' else 'j'
    return idx

def digit_average(valIJ,weights,digit):
    digit_average = (valIJ*weights).groupby(digit).sum() / \
            weights.groupby(digit).sum()
    return digit_average

def minmax(rean_list_minmax,var_name,sector):
    sector_min = sector[rean_list_minmax[0]+var_name]
    sector_max = sector[rean_list_minmax[0]+var_name]
    for rean in rean_list_minmax[1:]:
        sector_min = np.minimum(sector_min,sector[rean+var_name])
        sector_max = np.maximum(sector_max,sector[rean+var_name])
    return sector_min, sector_max


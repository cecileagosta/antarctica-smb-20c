#!/usr/bin/env python
# encoding: utf-8
'''
antarctica-20c-density.py
Created by Cécile Agosta on 30-06-2017
Last modified: 12-03-2018
2018 __ULiege__
'''
# = IMPORT MODULES ========================================================== #
# ipython -pylab for interactive plot
from __future__ import division # true division
from netCDF4 import Dataset
import datetime
import numpy as np # array module
import numpy.ma as ma # masked array
from StringIO import StringIO
from pylab import *
from time import * # module to measure time
import csv
from os.path import exists
from mpl_toolkits.axes_grid1 import host_subplot
from matplotlib.transforms import Bbox
from matplotlib.path import Path
from matplotlib.patches import Rectangle
from matplotlib.font_manager import FontProperties
from matplotlib import gridspec
import matplotlib.pyplot as plt
import operator
import array
import pandas as pd # Python Data Analysis Library
import scipy
from sklearn import linear_model
# My modules
import read_model
import extract_data
import projection
import readwrite
import extract_dem
import shade
# ========================================================================== #

# fixed variables #
# =============== #
mod_dir = './data/model/'
obs_dir = './data/observation/'
# time axis
version = 'v3.6.41'
year_range = range(1979,2015+1)
dates_index = np.array([pd.datetime(year,month,15) for year in year_range for month in np.array(range(12))+1])
years_mar = np.array([di.year for di in dates_index])
verbose = True
fw = 4.72441 # 12 cm in inches 

# read models #
# =========== #
# read MAR #
# -------- #
# grid
grid2D = read_model.read_mar_grid()
x1D, y1D = grid2D['x'], grid2D['y']
x2D, y2D = grid2D['x2D'], grid2D['y2D']
dh = (x1D[1] - x1D[0])/2.
lsm2D = (grid2D['ice']<0.3)
# mar density
filename = mod_dir+'MAR-JRA-55_RO1_20c_ave.nc4'
print "reading:",filename
# snowpack discretization
def _1Dto3D(z1D):
    x3D, z3D, y3D = np.meshgrid(y1D,z1D,x1D)
    return z3D
z_mid = np.array(readwrite.read_nc(filename,['outlay']))[0,:]
z_inf = np.append([0.],(z_mid[:-1]+z_mid[1:])/2.)
z_sup = np.append(z_inf[1:],[z_inf[-1]+5.])
dz1D = z_sup - z_inf
dz3D = _1Dto3D(dz1D)
# snowpack density
rho3D = ma.array(readwrite.read_nc(filename,['RO1'],lev=True))
rho3D = reshape(rho3D,rho3D.shape[1:])
lsm3D = np.array([lsm2D for tt in range(rho3D.shape[0])])
rho3D = ma.array(rho3D,mask=lsm3D)
# cumulative density
def rho2D_cum(zz,rho3D=rho3D,z_inf=z_inf,z_sup=z_sup,dz3D=dz3D):
    z_mask = z_inf<=zz
    fact1D = maximum(minimum((zz-z_inf)/(z_sup-z_inf),1.),0.)
    fact3D = _1Dto3D(fact1D)
    rho2D = (rho3D[z_mask,:,:]*dz3D[z_mask,:,:]*fact3D[z_mask,:,:]).sum(axis=0)\
            /(dz3D[z_mask,:,:]*fact3D[z_mask,:,:]).sum(axis=0)
    return rho2D
filename = mod_dir+"MAR-JRA-55_ST_20c_ave.nc4"
st2D = readwrite.read_nc(filename,['ST'])[0] + 273.15
filename = mod_dir+"MAR-JRA-55_UVz_20c_ave.nc4"
ws2D = readwrite.read_nc(filename,['UVz'])[0]

# read obs #
# ======== #
filename = obs_dir+'density_compilation_2018-03.csv'
print "reading:",filename
obs = pd.read_csv(filename,na_values=-9999)
obs['density'] = obs['density']*1000.
obs['x'], obs['y'] = projection.stereosouth_lonlat2xy(obs['lon'],obs['lat'])
lab_xy=['x'+str(round(obs['x'][name],1))+'y'+str(round(obs['y'][name],1)) for name in obs.index]
obs['label']=lab_xy
obs['sh_bedmap2'] = extract_dem.bedmap2(obs['lon'],obs['lat'])
obs.loc[np.isnan(obs['sh']),'sh']=obs['sh_bedmap2'][np.isnan(obs['sh'])]
obs['dz'] = obs['z_sup'] - obs['z_inf']
# project obs on MAR grid #
# ======================= #
gridXY, ijw_dict, mask_dict = extract_data.grid_2DtoXY(grid2D,obs,vtype='density')
obs['mar_i'], obs['mar_j'], obs['ij'] = gridXY['mar_i'], gridXY['mar_j'], gridXY['ij']
# filter observations #
# =================== #
# eliminate:
# - islands: lsm=1
mask = (gridXY['lsm']==1)
# - abs(sh - sh_bamber)> 150
dsh = abs(gridXY['sh']-obs['sh'])
mask = mask&(dsh<=150)
# - z_sup too deep
mask = mask&(obs['z_sup']<=2.)
mask = mask&(obs['z_inf']<=1.)
# - on the ice-sheet
mask = mask&(gridXY['nb_of_grid_points']>=3)
# apply filter
obs = obs[mask]
# cumulate obs for different depths

def mode(mylist):
    mylist = list(mylist)
    return max(set(mylist), key=mylist.count)

def obs_cum(zz):
    z_mask = obs['z_inf']<=zz
    fact = maximum(minimum((zz-obs['z_inf'])/(obs['z_sup']-obs['z_inf']),1.),0.)
    rho_cum = (obs['density'][z_mask]*obs['dz'][z_mask]*fact[z_mask]).groupby(obs['label']).sum()\
            /(obs['dz'][z_mask]*fact[z_mask]).groupby(obs['label']).sum()
    obs_cum = pd.DataFrame([[np.nan] for name in rho_cum.index],
                    index=rho_cum.index, columns=['rho'])
    obs_cum['rho'] = rho_cum
    obs_cum['z_sup'] = obs['z_sup'][z_mask].groupby(obs['label'][z_mask]).max()
    obs_cum['z_inf'] = obs['z_inf'][z_mask].groupby(obs['label'][z_mask]).min()
    var_remove = ('density','dz','z_sup','z_inf','z_mid','name')
    var_list = [var_name for var_name in list(obs) if var_name not in var_remove]
    for varname in var_list:
        obs_cum[varname] = obs[varname][z_mask].groupby(obs['label'][z_mask]).apply(mode)
    mask_cum = (obs_cum['z_sup']>=zz)&(obs_cum['z_sup']<=zz*2.)&(obs_cum['z_inf']<0.01)
    return obs_cum[mask_cum]


def IJbin(obsZZ):
    obs_bin = obsZZ.groupby('ij').mean()
    obs_bin['z_sup'] = obsZZ['z_sup'].groupby(obsZZ['ij']).max()
    obs_bin['z_inf'] = obsZZ['z_inf'].groupby(obsZZ['ij']).min()
    var_remove = ['rho','z_sup','z_inf','lon','lat','sh','x','y','sh_bedmap2']
    var_list = [var_name for var_name in list(obsZZ) if var_name not in var_remove]
    for varname in var_list:
        obs_bin[varname] = obsZZ[varname].groupby(obsZZ['ij']).apply(mode)
    return obs_bin

def var2DtoIJ(var2D,obsIJ):
    return np.array([var2D[mar_j-1,mar_i-1] for (mar_j,mar_i) in zip(obsIJ['mar_j'],obsIJ['mar_i'])])

# compare obs and model #
# ===================== #

obs02 = obs_cum(0.2)
obs05 = obs_cum(0.5)
obs1 = obs_cum(1)

rho2D02 = rho2D_cum(0.2)
rho2D05 = rho2D_cum(0.5)
rho2D1 = rho2D_cum(1.)

# personalise shading functions #
# ============================= #
def scontour(var2D,levels):
    return shade.scontour(x2D,y2D,var2D,levels)
def spcolor(var2D,cmap,bounds):
    return shade.spcolor(x2D,y2D,var2D,cmap,bounds)
def layout():
    return shade.layout(x2D,y2D,grid2D['lon'],grid2D['lat'],grid2D['sh'],lsm2D,grid2D['ground'])
cmap_seq, cmap_smb, cmap_div, cmap_qual, cmap_BuRd, obs_color, sh_color = shade.def_cmaps()

# plots #
# ===== #
def shade_scatter_rho(obsZZ,rho2DZZ,cmap=cmap_smb,cut=True,label='',figname='',figlab=''):
    obsIJ = IJbin(obsZZ)
    fig, gs = shade.set_fig()
    bounds = linspace(280,480,11)
    spcolor(rho2DZZ,cmap,bounds)
    scatter(obsIJ['x'],obsIJ['y'],c=obsIJ['rho'],cmap=cmap,\
            norm=shade.norm_cmap(cmap,bounds),edgecolor='k',linewidth=0.2,s=20,zorder=100)
    layout()
    lab = 'Snow density '+label+r' (kg m$^{-3}$)'
    shade.top_label(lab)
    shade.subfig_label(figlab,dx=50,dy=-50)
    shade.plot_cbar(gs,cmap,bounds)
    savefig('./fig/supplementary/figS1_map'+figname+'.png',dpi=600)

def scatterIJ(obsZZ,rho2DZZ,label='',reglin=False):
    obsIJ = IJbin(obsZZ)
    obsIJ = obsIJ[(obsIJ['rho']>250.)&(obsIJ['rho']<480.)]
    rho2D_IJ = var2DtoIJ(rho2DZZ,obsIJ)
    st2D_IJ = var2DtoIJ(st2D,obsIJ)
    ws2D_IJ = var2DtoIJ(ws2D,obsIJ)
    # compute new regression coefficient
    # initial formula
    # tt = 149.2 + 6.84*ws2D_IJ + 0.48*st2D_IJ
    # scatter
    if reglin:
        reg = linear_model.LinearRegression()
        X = np.array([st2D_IJ,ws2D_IJ]).transpose()
        y = np.array(obsIJ['rho'])
        reg.fit(X,y)
        intercept = reg.intercept_
        coef_st, coef_ws = reg.coef_
        print round(intercept,2), round(coef_st,2), round(coef_ws,2)
        rho2D_IJ = intercept + coef_st*st2D_IJ + coef_ws*ws2D_IJ
    ss,cc,rr,pp,stderr = scipy.stats.linregress(obsIJ['rho'],rho2D_IJ)
    bias = (rho2D_IJ - obsIJ['rho']).mean()/obsIJ['rho'].mean()
    # print int(round(bias*100,0))
    lab = label+r' ($y=$'+str(round(ss,1))+r'$\,x + $'+str(int(round(cc,0)))+\
            ', R2'+'$=$'+str(round(rr**2,1))+r', bias$=$'+str(int(round(bias*100,0)))+'%)'
    scatter(obsIJ['rho'],rho2D_IJ,label=lab,s=20)
    vminmax = np.array([minimum(obsIJ['rho'].min(),rho2D_IJ.min()), maximum(obsIJ['rho'].max(),rho2D_IJ.max())])
    plot(vminmax,ss*vminmax+cc,lw=2)

def figS1_scatterIJ(figlab='',reglin=False,save=True):
    fig = figure(figsize=(fw,fw))
    subplots_adjust(left=0.14,right=0.96,bottom=0.12,top=0.94)
    scatterIJ(obs02,rho2D02,label=r'0$-$0.2 m',reglin=reglin)
    scatterIJ(obs05,rho2D05,label=r'0$-$0.5 m',reglin=reglin)
    scatterIJ(obs1,rho2D1,label=r'0$-$1 m',reglin=reglin)
    plot([250,510],[250,510],c='grey',ls='-',lw=1,zorder=0)
    axis([250,510,250,510])
    xlabel(r'Observed snow density (kg m$^{-3}$)')
    ylabel(r'Modeled snow density (kg m$^{-3}$)')
    text(255,505,figlab,ha='left',va='top',fontsize='large')
    legend(loc='upper left',handletextpad=0,bbox_to_anchor=(0., 0.93))
    if save:
        savefig('./fig/supplementary/figS1_scatter.png',dpi=517)

def plot_regressions():
    fig = figure(figsize=(fw,fw))
    subplots_adjust(left=0.14,right=0.96,bottom=0.12,top=0.94)
    plot([250,510],[250,510],c='grey',ls='-',lw=1,zorder=0)
    axis([250,510,250,510])
    def plot_reg(intercept,coef_st,coef_ws,lab=''):
        tt = intercept + coef_st*st2D_IJ + coef_ws*ws2D_IJ
        ss,cc,rr,pp,stderr = scipy.stats.linregress(obsIJ['rho'],tt)
        scatter(obsIJ['rho'],tt,label=lab)
        plot(obsIJ['rho'],obsIJ['rho']*ss+cc)
        print round(ss,2),round(cc,1),round(rr**2,3), round((tt-obsIJ['rho']).mean(),1)
    # previous
    plot_reg(149.2,0.48,6.84,lab='ini')
    # new
    plot_reg(183.66,0.44,11.69,lab='reg0.2')
    plot_reg(149.68,0.56,13.74,lab='reg0.5') # 
    plot_reg(177.52,0.51,11.95,lab='reg1')
    # best compromise: same r2 + highest slope + reduced bias
    plot_reg(125,0.6,14,lab='best')
    legend()


def figS1():
    shade_scatter_rho(obs02,rho2D02,label=r'0$-$0.2 m',figname='02',figlab='(a)')
    shade_scatter_rho(obs05,rho2D05,label=r'0$-$0.5 m',figname='05',figlab='(b)')
    shade_scatter_rho(obs1,rho2D1,label=r'0$-$1 m',figname='1',figlab='(c)')
    figS1_scatterIJ(figlab='(d)')

def tableS1():
    lab_list = '20','50','100'
    obsZZ = {'20':obs02,'50':obs05,'100':obs1}
    obsIJ = {lab:IJbin(obsZZ[lab]) for lab in lab_list}
    nbZZ, nbIJ, ref_list = {}, {}, []
    for lab in lab_list:
        nbZZ[lab] = obsZZ[lab]['citekey'].groupby(obsZZ[lab]['citekey']).count()
        nbIJ[lab] = obsIJ[lab]['citekey'].groupby(obsIJ[lab]['citekey']).count()
        ref_list = ref_list+list(nbZZ[lab].keys())
    ref_list = sorted(list(set(ref_list)))
    for lab in list(nbZZ):
        rlistZZ = [ref for ref in ref_list if ref not in nbZZ[lab].keys()]
        rlistIJ = [ref for ref in ref_list if ref not in nbIJ[lab].keys()]
        for ref in rlistZZ:
            nbZZ[lab][ref] = 0
        for ref in rlistIJ:
            nbIJ[lab][ref] = 0
    add_lab = {'20':' & ','50':' & ','100':' \\\\'}
    print_nb = {}
    for ref in ref_list:
        print_nb[ref] = ''
        for lab in lab_list:
            print_nb[ref] = print_nb[ref]+str(nbZZ[lab][ref])+'/'+str(nbIJ[lab][ref])+add_lab[lab]
    datasets = dict(obs['dataset_name'].groupby(obs['citekey']).first())
    print '\\tophline'
    print 'Reference & Dataset & 0--20 cm & 0--50 cm & 0--100 cm \\\\'
    print '\\middlehline'
    for ref in ref_list:
        print '\\citet{'+ref+'}'+' & '+datasets[ref]+' & '+print_nb[ref]
    print '\\bottomhline'




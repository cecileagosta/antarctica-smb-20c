#!/usr/bin/env python
# encoding: utf-8
'''
extract_data.py
Created by Cécile Agosta: may 2016
Last modified: 22 oct 2017
__ULiege__
'''
# = IMPORT MODULES ========================================================== #
from __future__ import division # true division
import operator
import pandas as pd # Python Data Analysis Library
import numpy as np # array module
from numpy import *
import netCDF4 as nc # netcdf module
from netCDF4 import Dataset
from os.path import exists
import projection
# ========================================================================== #

def compute_dh(xx,yy,verbose=False):
    # Look for stereo grid characteristics
    if round(yy[1]-yy[0],3) == round(xx[1]-xx[0],3):
            dh = round(xx[1]-xx[0],3)
            if verbose:
                print 'Resolution : ',dh,' km'
    else:
            print 'ERROR : Not same resolution in X and Y dimensions (??)',round(yy[1]-yy[0],5)-round(xx[1]-xx[0],5)
    return dh

def xy2ijw(xx,yy,mod_grid):
    dh, x0, y0 = mod_grid
    # lower left grid mesh center of the grid the closest to (xx,yy)
    i0,j0 = int((xx-x0)/dh),int((yy-y0)/dh)
    xg,yg = x0+i0*dh,y0+j0*dh
    # area weights for the 4 closest grid mesh centers
    dx,dy = xx-xg,yy-yg
    areax,areay = [dh-dx,dx],[dh-dy,dy]
    add_ij = [[0,0],[1,0],[0,1],[1,1]]
    col_names = 'i', 'j', 'w'
    ijw = pd.DataFrame([[np.nan for col in col_names] for mesh in range(4)], columns=col_names)
    for nb,[aii,ajj] in enumerate(add_ij):
        ijw['i'][nb] = i0+aii
        ijw['j'][nb] = j0+ajj
        ijw['w'][nb] = (areax[aii]*areay[ajj])/(dh*dh)
    ijw = ijw.sort_values('w',ascending=0)
    ijw.reset_index(drop=True,inplace=True)
    return ijw

def lonlat2ijw(lon,lat,mod_grid):
    xx,yy = projection.stereosouth_lonlat2xy([lon],[lat])
    xx,yy = xx[0],yy[0]
    if np.isnan(xx) or np.isnan(yy) :
        print 'NaN in extract.lonlat2ijw : lon ',lon,', lat ',lat
    ijw = xy2ijw(xx,yy,mod_grid)
    return ijw

def itp_2DtoXY_ijw(obs_index,obs_lon,obs_lat,mod_x,mod_y,vtype=''):
    ijwfile='./data/out/itp_ijw'+vtype+'.npy'
    if exists(ijwfile):
        ijw_dict = np.load(ijwfile)[()]
    else:
        mod_grid = compute_dh(mod_x,mod_y), mod_x[0], mod_y[0]
        obs_on_mod = []
        obs_on_mod_ij = []
        ijw_dict = {}
        for name in obs_index:
            if np.isnan(obs_lon[name]) or np.isnan(obs_lat[name]) :
                print 'NaN in extract_mod_meshes_on_obs : obs ',name
            ijw_dict[name] = lonlat2ijw(obs_lon[name],obs_lat[name],mod_grid)
        np.save(ijwfile,ijw_dict)
    return ijw_dict

def itp_2DtoXY_mask(ijw_dict,obs_index,obs_sh,mar2D_sh,mar2D_lsm,dsh_max=200.,vtype=''):
    maskfile='./data/out/itp_mask'+vtype+'.npy'
    if exists(maskfile):
        mask_dict = np.load(maskfile)[()]
    else:
        mask_dict = {}
        for name in obs_index:
            ijw = ijw_dict[name]
            ij = zip(ijw['i'].astype(int),ijw['j'].astype(int))
            ijw['lsm'] = [mar2D_lsm[j,i] for i,j in ij]
            ijw['sh'] = [mar2D_sh[j,i] for i,j in ij]
            dsh = abs(ijw['sh']-obs_sh[name])
            mask = (ijw['lsm']==1)&(dsh<dsh_max)
            if mask.sum()==0 or ijw['w'][mask].sum()<0.01:
                # just take the closest grid point
                mask = pd.Series([True,False,False,False],index=ijw.index)
            mask_dict[name] = mask
        np.save(maskfile,mask_dict)
    return mask_dict

def itp_2DtoXY(var,ijw,mask):
    nijw = ijw[mask]
    w_tot = 0.
    w_var = 0.
    for idx in nijw.index:
        ii = int(nijw['i'].loc[idx])
        jj = int(nijw['j'].loc[idx])
        ww = nijw['w'].loc[idx]
        w_tot += ww
        if var.ndim==2:
            w_var += var[jj,ii]*ww
        elif var.ndim==3:
            w_var += var[:,jj,ii]*ww
        else:
            print 'ERROR in itp_2DtoXY: var must have ndim == 2 or 3'
    varXY = w_var/w_tot
    return varXY

def grid_2DtoXY(grid2D,obs,vtype=''):
    # interpolation fields
    ijw_dict = itp_2DtoXY_ijw(obs.index, obs['lon'], obs['lat'], grid2D['x'], grid2D['y'],vtype=vtype)
    mask_dict = itp_2DtoXY_mask(ijw_dict,obs.index,obs['sh'],grid2D['sh'],grid2D['lsm'],dsh_max=200.,vtype=vtype)
    # model time independent variables
    gridfile = './data/out/gridXY'+vtype+'.csv'
    if exists(gridfile):
         print 'loading gridXY : '+gridfile
         gridXY = pd.read_csv(gridfile)
         gridXY = gridXY.set_index(['name'])
         gridXY['x'], gridXY['y'] = obs['x'], obs['y']
    else:
        grid_var = list(grid2D)
        for varname in 'x','y','lon','lat':
            grid_var.remove(varname)
        gridXY = pd.DataFrame([[np.nan for var in grid_var] for name in obs.index],
                        index=obs.index, columns=grid_var)
        nb, ijlab, mar_i, mar_j, lon_mar, lat_mar = [], [], [], [], [], []
        for idx,name in enumerate(obs.index):
            print idx,name
            ijw = ijw_dict[name]
            mask = mask_dict[name]
            nijw = ijw[mask]
            jnr, inr = int(nijw['j'].iloc[0]), int(nijw['i'].iloc[0]) # nearest grid point on ice mask
            for var in grid_var:
                if var=='basin':
                    gridXY[var][name] = grid2D[var][jnr,inr] # nearest grid point on ice mask
                else:
                    gridXY[var][name] = itp_2DtoXY(grid2D[var],ijw,mask)
            nb.append(len(ijw[mask]))
            ijlab.append(str(inr+1)+'x'+str(jnr+1))
            mar_i.append(inr+1)
            mar_j.append(jnr+1)
        llon, llat = projection.stereosouth_xy2lonlat(gridXY['x2D'],gridXY['y2D'])
        gridXY['name'] = obs.index
        gridXY['lon'] = llon
        gridXY['lat'] = llat
        gridXY['ij'] = ijlab
        gridXY['nb_of_grid_points'] = nb
        gridXY['mar_i'] = mar_i
        gridXY['mar_j'] = mar_j
        gridXY['x'], gridXY['y'] = obs['x'], obs['y']
        print('writing gridXY : '+gridfile)
        gridXY.to_csv(gridfile)
    return gridXY, ijw_dict, mask_dict

def model_2DtoXY(rean,ijw_dict,mask_dict,smb2D,model_dtime,obs):
    # model time-dependent variables
    modelsmbfile = './data/out/smb_'+rean+'.csv'
    if exists(modelsmbfile):
        print('loading: '+modelsmbfile)
        smbXY = pd.read_csv(modelsmbfile)
        smbXY = smbXY.set_index(['name'])
    else:
        smbXY = pd.DataFrame([[np.nan  for col in range(2)] for name in obs.index],index=obs.index,columns=['mean','std'])
        model_year = np.array([date.year for date in model_dtime[rean]])
        for idx, name in enumerate(obs.index):
            print idx,name
            # spatial interpolation
            smbXYT = itp_2DtoXY(smb2D[rean],ijw_dict[name],mask_dict[name])
            # temporal interpolation
            yearstart, yearend = obs['yearstart'][name], obs['yearend'][name]
            yearnb = yearend - yearstart + 1
            if yearstart>=1979:
                tmask = (model_year>=yearstart)&(model_year<=yearend)
            else:
                if yearnb>=8:
                    tmask = (model_year>=yearstart) # True
                    # could be improved by taking a mean of yearnb means, but well ...
                else:
                    tmask = (model_year<=yearstart) # False
            if tmask.any():
                smbXY['mean'][name] = smbXYT[tmask].mean()
                annual_smb = np.array([smbXYT[model_year==year].mean() for year in set(model_year[tmask])])
                smbXY['std'][name] = annual_smb.std()
        print('writing: '+modelsmbfile)
        smbXY.to_csv(modelsmbfile)
    return smbXY

def Tmodel_to_Tobs(rean,smbXY,obs,years_model):
    modelmean,modelstd = [],[]
    for name in obs.index:
        modeldata = smbXY[rean][name]
        modelstd.append(modeldata.groupby(years_model).mean().std())
        if obs['yearstart'][name]>=1979:
            year_select = (years_model>=obs['yearstart'][name])&(years_model<=obs['yearend'][name])
            modelmean.append(modeldata[year_select].mean())
        else:
            modelmean.append(modeldata.mean())
    smbXYT_mean = pd.DataFrame(modelmean, index=obs.index)
    smbXYT_std = pd.DataFrame(modelstd, index=obs.index)
    return smbXYT_mean,smbXYT_std

def define_regions():
    basin_name = {0:'island',1:'H-Hp',2:'F-G',3:'E-Ep',4:'D-Dp',5:'Cp-D',6:'B-C',7:'A-Ap',8:'Jpp-K',9:'G-H',10:'Dp-E'\
                ,11:'Ap-B',12:'C-Cp',13:'K-A',14:'J-Jpp',15:'Ipp-J',16:'I-Ipp',17:'Hp-I',18:'Ep-F',-999:'shelf'}
    location_name = {'Hp-I':'apis','I-Ipp':'apis','Ipp-J':'apis',\
            'Ep-F':'wais','F-G':'wais','G-H':'wais','H-Hp':'wais','J-Jpp':'wais',\
            'Jpp-K':'eais','K-A':'eais','A-Ap':'eais','Ap-B':'eais','B-C':'eais',\
            'C-Cp':'eais','Cp-D':'eais','D-Dp':'eais','Dp-E':'eais','E-Ep':'eais',\
            'shelf':'shelf','island':'island'}
    region_name = {'Ep-F':'Mary Byrd Land','F-G':'Amundsen Sea','G-H':'Amundsen Sea','H-Hp':'Amundsen Sea',\
            'Hp-I':'Peninsula','I-Ipp':'Peninsula','Ipp-J':'Peninsula','J-Jpp':'Ronne basin','Jpp-K':'Filchner basin',\
            'K-A':'Dronning Maud Land','A-Ap':'Dronning Maud Land','Ap-B':'Dronning Maud Land',\
            'B-C':'Amery basin','C-Cp':'Wilkes Land','Cp-D':'Wilkes Land','D-Dp':'Wilkes Land',\
            'Dp-E':'Ross Transantarctics','E-Ep':'Ross Transantarctics','island':'Island','shelf':'Ice-Shelf'}
    return basin_name, location_name, region_name

def check_select(obs,dsh,startbefore79andnblt8,modellsm,suspicious):
    print
    selected=(obs['smb']!=0.)&(dsh<=200.)&(modellsm==1)&\
            logical_not(startbefore79andnblt8)&logical_not(suspicious)
    check = 0
    print 'smb=0:', (obs['smb']==0.).sum()
    check += (obs['smb']==0.).sum()
    print 'dsh>200:', (dsh>200.).sum()
    check += (dsh>200.).sum()
    print 'climato not ok:', startbefore79andnblt8.sum()
    check += startbefore79andnblt8.sum()
    print 'lsm==0:', (modellsm==0).sum()
    check += (modellsm==0).sum()
    print 'suspicious:', suspicious.sum()
    check += suspicious.sum()
    print 'Total removed:', selected.shape[0]-selected.sum()
    print 'Sum of contraints:', check

def digit_average(val,weights,digit):
    digit_average = (val*weights).groupby(digit).sum() / \
            weights.groupby(digit).sum()
    return digit_average

def grid_XYtoIJ(obs,gridXY,smbXY,grid2D):
    # define regions
    basin_name, location_name, region_name = define_regions()
    gridIJfile = './data/out/gridIJ_obs-vs-model.csv'
    if exists(gridIJfile):
        gridIJ = pd.read_csv(gridIJfile)
        gridIJ = gridIJ.set_index(['ij'])
    else:
        weights = np.minimum(obs['yearnb'],obs['yearend']-obs['yearstart']+1)
        digit = gridXY['ij']
        gridIJ = pd.DataFrame(gridXY['basin'].groupby(digit).first(),columns=['basin'])
        gridIJ['basin_name'] = [basin_name[int(basin)] for basin in gridIJ['basin']]
        gridIJ['location'] = [location_name[basin_name] for basin_name in gridIJ['basin_name']]
        gridIJ['region'] = [region_name[basin_name] for basin_name in gridIJ['basin_name']]
        # compute values on IJ grid cells
        gridIJ['database'] = obs['database'].groupby(digit).first()
        for var_name in 'smb','sh','x','y':
            gridIJ['obs_'+var_name] = digit_average(obs[var_name],weights,digit)
        gridIJ['obs_smb_std'] = obs['smb'].groupby(digit).std()
        gridIJ['obs_sh_std'] = obs['sh'].groupby(digit).std()
        gridIJ['obs_nb'] = obs['smb'].groupby(digit).count()
        gridIJ['obs_lon'], gridIJ['obs_lat'] = projection.stereosouth_xy2lonlat(gridIJ['obs_x'],gridIJ['obs_y'])
        gridIJ['yearstart'] = obs['yearstart'].groupby(digit).min()
        gridIJ['yearend'] = obs['yearend'].groupby(digit).max()
        gridIJ['yearnb'] = obs['yearnb'].groupby(digit).sum()
        for rean in list(smbXY):
            gridIJ[rean+'_smb'] = digit_average(smbXY[rean]['mean'],weights,digit)
            gridIJ[rean+'_smb_std'] = smbXY[rean]['std'].groupby(digit).mean()
        # mar
        var_list = 'sh','curv','lsm','ground','ice','area','basin','nb_of_grid_points'
        for var_name in var_list:
            gridIJ['mar_'+var_name] = digit_average(gridXY[var_name],weights,digit)
        # racmo2 and others
        for var_name in 'rac_sh','rac_curv','ws','subl','div','curv':
            gridIJ[var_name] = digit_average(gridXY[var_name],weights,digit)
        gridIJ['mar_i'] = gridXY['mar_i'].groupby(digit).first()
        gridIJ['mar_j'] = gridXY['mar_j'].groupby(digit).first()
        gridIJ['x'] = np.array([grid2D['x'][ii] for ii in gridIJ['mar_i']-1])
        gridIJ['y'] = np.array([grid2D['y'][jj] for jj in gridIJ['mar_j']-1])
        llon, llat = projection.stereosouth_xy2lonlat(gridIJ['x'],gridIJ['y'])
        gridIJ['lon'] = llon
        gridIJ['lat'] = llat
        gridIJ.to_csv('./data/out/gridIJ_obs-vs-model.csv')
    return gridIJ
